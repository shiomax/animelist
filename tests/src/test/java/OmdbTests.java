import api.OmdbClient;
import apicontracts.ApiClient;
import apicontracts.KeyValueStore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OmdbTests {

    @Test
    void noApiKey() {
        ApiClient apiClient = new OmdbClient();
        var result = apiClient.search("Game of Thrones");

        assertFalse(result.isSuccess());
    }

    @Test
    void settingsTest() {
        var someKey = "key123";
        ApiClient apiClient = new OmdbClient();

        var setting = apiClient.getApiSettings().get(0).getKey();
        apiClient.persistSetting(setting, someKey);

        var settingStore = KeyValueStore.instanceOf();

        assertEquals(someKey, settingStore.get(setting));
    }

    @Test
    void searchTest() {
        var apiKey = System.getenv("OMDB_API_KEY");

        if(apiKey != null) {
            ApiClient apiClient = new OmdbClient();
            var setting = apiClient.getApiSettings().get(0).getKey();
            apiClient.persistSetting(setting, apiKey);

            var result = apiClient.search("Game of Thrones");

            assertTrue(result.isSuccess());
            assertTrue(result.getValue().size() > 0);
        }
    }
}
