import api.AniListClient;
import apicontracts.ApiClient;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AnilistTests {

    @Test
    void apiSearch() {
        ApiClient apiClient = new AniListClient();
        var result = apiClient.search("Naruto");

        assertTrue(result.isSuccess());
        assertTrue(result.getValue().size() > 0);
    }
}
