package apicontracts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

public class KeyValueStore {

    private KeyValueStore() {
        this.properties = new Properties();
        readFromFile();
    }

    private static KeyValueStore instance = null;

    public static KeyValueStore instanceOf() {
        return instance == null ? (instance = new KeyValueStore()) : instance;
    }

    private Properties properties;

    public final String get(String key) {
        if(properties == null || key == null || key.isEmpty())
            return "";
        return this.properties.containsKey(key) ? (String)this.properties.get(key) : "";
    }

    public final void persist(String key, String value) {
        properties.setProperty(key, value);
        writeToFile();
    }

    public final void remove(String key) {
        if(properties.containsKey(key)) {
            properties.remove(key);
            writeToFile();
        }
    }

    private static final String FILE_NAME = "plugins.config";

    private final void readFromFile() {
        if(Files.exists(Paths.get(FILE_NAME))) {
            Properties prop = new Properties();
            boolean error = false;
            try (FileInputStream inputStream = new FileInputStream(FILE_NAME)) {
                prop.load(inputStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                error = true;
            } catch (IOException e) {
                e.printStackTrace();
                error = true;
            }
            if(!error) {
                for(Map.Entry<Object, Object> cur : prop.entrySet())
                    cur.setValue(flip((String)cur.getValue()));
                properties = prop;
            }
        }
    }

    private final void writeToFile() {
        Properties prop = new Properties();
        for(Map.Entry<Object, Object> cur : properties.entrySet())
            prop.setProperty((String)cur.getKey(), flip((String)cur.getValue()));
        try(FileOutputStream outputStream = new FileOutputStream(FILE_NAME)) {
            prop.store(outputStream, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final String flip(String str) {

        final String KEY = "1ifLWh28fHFHoawd102##h9hfag72g6dfIWUHDGas125gbasdD";

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < str.length(); i++) {
            char value = (char)(str.charAt(i) ^ KEY.charAt(i % (KEY.length() - 1)));
            sb.append(value);
        }

        return sb.toString();
    }
}
