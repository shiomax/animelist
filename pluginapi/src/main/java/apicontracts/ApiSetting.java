package apicontracts;

public class ApiSetting {

    private String displayValue, key;
    private SettingDisplayType displayType;

    public ApiSetting(String displayValue, String key, SettingDisplayType displayType) {
        this.displayValue = displayValue;
        this.key = key;
        this.displayType = displayType;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getKey() {
        return key == null ? "" : key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public SettingDisplayType getDisplayType() {
        return displayType;
    }

    public void setDisplayType(SettingDisplayType displayType) {
        this.displayType = displayType;
    }
}
