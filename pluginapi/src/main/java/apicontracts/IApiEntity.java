package apicontracts;

import java.net.URL;
import java.util.List;

public interface IApiEntity {

    /*
    * List of options for name and alternative names
    * */
    List<String> getNameOptions();

    /*
    * List of Artwork options.
    * */
    List<String> getImageOptions();

    /*
    * List of description options
    * */
    List<String> getDescriptionOptions();
}
