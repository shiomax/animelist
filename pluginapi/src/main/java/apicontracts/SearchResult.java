package apicontracts;

import java.util.List;

public class SearchResult {

    private List<IApiEntity> value;

    private boolean success;

    private String errorMessage;

    public SearchResult(List<IApiEntity> value, boolean success, String errorMessage) {
        this.value = value;
        this.success = success;
        this.errorMessage = errorMessage;
    }

    public List<IApiEntity> getValue() {
        return this.value;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
