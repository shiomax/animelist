package apicontracts;

import java.util.List;

public abstract class ApiClient {

    private List<ApiSetting> apiSettings;

    /*
    * Constructor takes settings (can be null if none).
    * */
    protected ApiClient(List<ApiSetting> apiSettings) {
        this.apiSettings = apiSettings;
    }

    protected ApiClient() {
        this(null);
    }

    protected final String getSetting(String key) {
        if(key == null)
            return "";
        return KeyValueStore.instanceOf().get(buildKey(key));
    }

    protected final String getSetting(ApiSetting apiSetting) {
        if(apiSetting == null)
            return "";
        return this.getSetting(apiSetting.getKey());
    }

    private final String buildKey(String key) {
        return String.format("%s_%s", this.getIdentifier(), key);
    }

    /*
    * Searches for Entries and returns a List of apicontracts.IApiEntity
    * */
    public abstract SearchResult search(String searchString);

    /*
    * Identifier used to retrieve and persist settings
    * */
    public abstract String getIdentifier();

    /*
    * Display name displayed to the User
    * */
    public abstract String getDisplayName();

    public final List<ApiSetting> getApiSettings() {
        return this.apiSettings;
    }

    protected final void setApiSettings(List<ApiSetting> apiSettings) {
        this.apiSettings = apiSettings;
    }

    /*
    * Persists a setting value.
    * */
    public final void persistSetting(String key, String settingValue) {
        if(this.isValidSetting(key, settingValue)) {
            KeyValueStore.instanceOf().persist(key, settingValue);
        }
    }

    protected final void persistSetting(ApiSetting apiSetting, String settingValue) {
        this.persistSetting(apiSetting.getKey(), settingValue);
    }

    /*
    * Before a Setting is saved this is used to test wether or not it's valid.
    * If invalid it won't be saved.
    * By default every setting is accepted, unless overwritten;
    * */
    public boolean isValidSetting(String key, String settingValue) {
        return true;
    }
}
