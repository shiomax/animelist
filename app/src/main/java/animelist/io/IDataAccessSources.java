package animelist.io;

import animelist.types.AnimeSource;
import animelist.types.AnimeSourceEntity;
import animelist.types.ContentType;

import java.util.List;

public interface IDataAccessSources {

    /*
    * gets all the sources for a specific anime
    * */
    List<AnimeSource> getSources(long animeId);

    /*
    * gets all the globalSources for an anime with a specific content type
    * and the once that fit any content type
    * */
    List<AnimeSource> getGlobalSources(ContentType contentType);

    /*
    * gets all the globalSources
    * */
    List<AnimeSource> getAllGlobalSources();

    /*
    * Persists a new source
    * */
    AnimeSourceEntity putSource(AnimeSourceEntity animeSource);

    /*
    * Update existing source
    * */
    void updateSource(AnimeSource animeSource);

    /*
    * Deletes a Source
    * */
    void deleteSource(long sourceId);

    /*
    * When an animes get´s deleted this is called to cleanup sources
    * */
    void deleteWithAnimeId(long animeId);

    /*
    * Deletes all the specific sources
    * */
    void deleteAll(boolean keepGlobal);
}
