package animelist.io;

import animelist.types.Anime;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;

import java.io.File;
import java.net.URL;

/*
* Interface for DataAccess, may or may not be implemented multiple times, i don´t know yet.
*
* This is not designed to be very reuseable, but rather tailored to provide functions to
* this application specifically.
* */
public interface IDataAccess {

    /*
    * Lists to cache animes.
    *
    * ObservableList<Anime> = FXCollections.observableArrayList()
    * FilteredList<Anime> = FilteredList<Anime>(ObservableList)
    * SortedList<Anime> = SortedList<Anime>(filteredList)
    *
    * Really there exists only one single List, the rest are just different
    * views of the very same List.
    *
    * One of the three methods is expected to be run before anything else,
    * as the controllers are designed around caching mostly everything and
    * other methods might not check, if it has already been loaded.
    * */
    ObservableList<Anime> getAll();

    SortedList<Anime> getAllSorted();

    FilteredList<Anime> getAllFiltered();

    /*
    * Returns size of the library
    * */
    SimpleIntegerProperty librarySize();

    /*
    * Selects anime with a specific id
    * */
    Anime getById(long id);

    /*
    * Various add Anime Methods
    * */
    long addAnime(Anime anime);

    long addAnime(Anime anime, File artwork); //This is really just a shortcut, normally artwork is to be managed with, the Artwork methods

    /*
    * Various update Anime Methods
    * */
    Anime updateAnime(Anime anime);

    /*
    * Various Delete Anime Methods
    * */
    void deleteAnime(Anime anime);

    void deleteAnime(long id);

    /*
    * Artwork specific Methods. Artwork is managed seperately as all the rest
    * is constantly cached and it´s probably a bad idea to put a ton
    * of Images that may or may not be used into RAM at all times
    * */
    ImageResponse getArtwork(Anime anime);

    ImageResponse getArtwork(long id);

    void updateAnimeArtworkFromFile(Anime anime, File artwork);

    void updateAnimeArtworkFromURL(Anime anime, URL url);

    void updateAnimeArtworkFromStrURL(Anime anime, String url);

    /*
    * This will remove the entire library.
    * */
    boolean resetLirary(boolean keepGlobalSources);
}