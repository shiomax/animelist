package animelist.io;

import javafx.scene.image.Image;

/*
* This is used for return values of Images in the DbController.
* */
public class ImageResponse {

    private Image image; //image for display
    private String path; //path of the Image
    private boolean defaultImage; //true if default image is used

    public ImageResponse(Image image, String path, boolean defaultImage) {
        this.image = image;
        this.path = path;
        this.defaultImage = defaultImage;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(boolean defaultImage) {
        this.defaultImage = defaultImage;
    }
}
