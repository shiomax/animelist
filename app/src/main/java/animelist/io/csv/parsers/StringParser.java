package animelist.io.csv.parsers;

import animelist.io.csv.ICellParser;

public class StringParser implements ICellParser<String> {

    /*
    * Replacements for to not break CSV formatting, with random strings.
    * */
    private final String[][] SUBSTITUTES = {
            {"\n", "<br>"}, //Line Break sub
            {";", "<cb>"} //Cell Break sub
    };


    @Override
    public Class getGenericClass() {
        return String.class;
    }

    @Override
    public boolean isValidFor(Class subject) {
        return subject == String.class;
    }

    @Override
    public boolean isValidFor(Object obj) {
        return obj instanceof String;
    }

    @Override
    public String parse(String str) {
        for(var sub : SUBSTITUTES) {
            str = str.replace(sub[1], sub[0]);
        }
        return str;
    }

    @Override
    public String toCsvCell(String str) {
        for(var sub : SUBSTITUTES) {
            str = str.replace(sub[0], sub[1]);
        }
        return str;
    }
}
