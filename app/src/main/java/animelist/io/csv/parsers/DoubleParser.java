package animelist.io.csv.parsers;

import animelist.io.csv.ICellParser;

public class DoubleParser implements ICellParser<Double> {

    @Override
    public Class getGenericClass() {
        return Double.class;
    }

    @Override
    public boolean isValidFor(Class subject) {
        return subject == double.class || subject == Double.class;
    }

    @Override
    public boolean isValidFor(Object obj) {
        return obj instanceof Double;
    }

    @Override
    public Double parse(String str) {
        return str.matches("\\d+(.\\d+)?") ? Double.valueOf(str) : 0.0;
    }

    @Override
    public String toCsvCell(Double obj) {
        return String.valueOf(obj);
    }
}
