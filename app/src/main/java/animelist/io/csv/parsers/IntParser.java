package animelist.io.csv.parsers;

import animelist.io.csv.ICellParser;

public class IntParser implements ICellParser<Integer> {
    @Override
    public Class getGenericClass() {
        return Integer.class;
    }

    @Override
    public boolean isValidFor(Class subject) {
        return subject == Integer.class || subject == int.class;
    }

    @Override
    public boolean isValidFor(Object obj) {
        return obj instanceof Long;
    }

    @Override
    public Integer parse(String str) {
        return str.matches("^-?\\d*\\.{0,1}\\d+$") ? Integer.valueOf(str) : 0;
    }

    @Override
    public String toCsvCell(Integer obj) {
        return String.valueOf(obj);
    }
}
