package animelist.io.csv.parsers;

import animelist.io.csv.ICellParser;

public class LongParser implements ICellParser<Long> {
    @Override
    public Class getGenericClass() {
        return Long.class;
    }

    @Override
    public boolean isValidFor(Class subject) {
        return subject == Long.class || subject == long.class;
    }

    @Override
    public boolean isValidFor(Object obj) {
        return obj instanceof Long;
    }

    @Override
    public Long parse(String str) {
        return str.matches("\\d+") ? Long.valueOf(str) : 0;
    }

    @Override
    public String toCsvCell(Long obj) {
        return String.valueOf(obj);
    }
}
