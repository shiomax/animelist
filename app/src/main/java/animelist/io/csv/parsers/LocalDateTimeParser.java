package animelist.io.csv.parsers;

import animelist.io.csv.ICellParser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalDateTimeParser implements ICellParser<LocalDateTime> {

    /*
     * Date Time formatters to parse dates that might be formatted crappy cause
     * guess what people use excel for csv and excel does not care about your formatting
     * */
    private final DateTimeFormatter[] AVAILABLE_FORMATTERS = {
            DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"), //index 0 will be used for saving
            DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
    };

    @Override
    public Class getGenericClass() {
        return LocalDateTime.class;
    }

    @Override
    public boolean isValidFor(Class subject) {
        return subject == LocalDateTime.class;
    }

    @Override
    public boolean isValidFor(Object obj) {
        return obj instanceof LocalDateTime;
    }

    @Override
    public LocalDateTime parse(String str) {
        LocalDateTime localDateTime = null;

        for(var formatter : AVAILABLE_FORMATTERS) {
            try {
                localDateTime = LocalDateTime.parse(str, formatter);
                break; //parsing complete
            } catch (DateTimeParseException e) { }
        }

        //failed to find suitable formatter
        if(localDateTime == null) {
            localDateTime = LocalDateTime.now();
        }

        return localDateTime;
    }

    @Override
    public String toCsvCell(LocalDateTime obj) {
        return obj.format(AVAILABLE_FORMATTERS[0]);
    }
}
