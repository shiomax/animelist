package animelist.io.csv.parsers;

import animelist.io.csv.ICellParser;

public class BooleanParser implements ICellParser<Boolean> {

    private static final String[] TRUE_VALUES = { "true", "1" };

    @Override
    public Class getGenericClass() {
        return Boolean.class;
    }

    @Override
    public boolean isValidFor(Class subject) {
        return subject == boolean.class || subject == Boolean.class;
    }

    @Override
    public boolean isValidFor(Object obj) {
        return obj instanceof Boolean;
    }

    @Override
    public Boolean parse(String str) {
        var lower = str.toLowerCase();
        for(var cur : TRUE_VALUES)
            if(cur.equals(lower))
                return true;
        return false;
    }

    @Override
    public String toCsvCell(Boolean obj) {
        return obj ? "1" : "0";
    }
}
