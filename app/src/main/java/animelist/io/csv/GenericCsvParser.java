package animelist.io.csv;

import animelist.io.csv.parsers.*;
import org.pmw.tinylog.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GenericCsvParser<T> {

    private Class<T> genericType;
    private Field[] fields;

    private static final String SEPERATOR = ";";

    private static final ICellParser[] CELL_PARSERS = {
            new StringParser(),
            new LocalDateTimeParser(),
            new DoubleParser(),
            new LongParser(),
            new BooleanParser(),
            new IntParser()
    };

    public GenericCsvParser(Class<T> genericType) {
        this.genericType = genericType;

        this.fields = genericType.getDeclaredFields();

        for (int i = 0; i < fields.length; i++)
            this.fields[i].setAccessible(true);
    }

    /*
     * Parses List of Objects to CSV-formatted-String
     * */
    public String toCsv(List<T> list) {
        StringBuilder result = new StringBuilder();

        //parse headers
        result.append(parseCsvHeader());
        result.append("\n");

        //parse fields
        for (var cur : list) {
            result.append(convertToCsvLine(cur));
            result.append("\n");
        }

        return result.toString();
    }

    public String parseCsvHeader() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            result.append(fields[i].getName());
            if (i != fields.length - 1)
                result.append(SEPERATOR);
        }
        return result.toString();
    }

    public String convertToCsvLine(T lineObj) {
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            try {
                line.append(convertFieldToCsvCell(fields[i].get(lineObj)));
            } catch (IllegalAccessException e) {
                Logger.error("Error in CSV parser method convertToCsvLine: " + e.getMessage());
                e.printStackTrace();
            }
            if (i != fields.length - 1)
                line.append(SEPERATOR);
        }
        return line.toString();
    }

    /*
     * Converts a field to a CSV-Cell
     * */
    private String convertFieldToCsvCell(Object obj) {
        if(obj == null)
            return "";

        var parser = Arrays.stream(CELL_PARSERS)
                .filter(val -> val.isValidFor(obj))
                .findAny();

        if(parser.isPresent()) {
            return parser.get().toCsvCell(obj);
        } else {
            return obj.toString();
        }
    }

    /*
     * Parses Entire CSV String to List<T>, requires T to have default constructor, or will fail
     * miserably.
     * */
    public List<T> parse(String str) {

        if (str == null)
            return null;

        String[] lbSplit = str.split("\n");

        if (lbSplit.length == 0)
            return null;

        this.headerMapper = genHashMapForMapping(lbSplit[0].split(SEPERATOR));
        List<T> result = new LinkedList<>();

        for (int i = 1; i < lbSplit.length; i++) {

            var parsed = this.parseCsvLine(lbSplit[i]);

            if(parsed != null)
                result.add(parsed);
            //TODO: else discarded a line warning
        }

        return result;
    }

    public T parseCsvLine(String line) {
        var cells = line.split(SEPERATOR);
        var iterator = headerMapper.entrySet().iterator();

        //create new instance
        T newInstance = instanceOfGenericType();

        boolean parsedSomething = false;

        while (iterator.hasNext()) {
            var index = iterator.next();

            //the index can be out of range in case there are cells missing, that are in the header
            if(index.getValue() >= cells.length)
                break;

            var field = this.fields[index.getKey()];

            //need seperate approach for enums as the class won´t match Enum.class, but rather the specific MyEnum.class
            if (field.getType().isEnum()) {
                try {
                    fields[index.getKey()].set(newInstance,
                            Enum.valueOf((Class<Enum>) field.getType(), cells[index.getValue()])
                    );
                    parsedSomething = true;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                var parser = Arrays.stream(CELL_PARSERS)
                        .filter(val -> val.isValidFor(field.getType()))
                        .findAny(); //does not matter witch one, there should be only one or none

                if (parser.isPresent()) {
                    try {
                        field.set(newInstance,
                                parser.get().getGenericClass().cast(parser.get().parse(cells[index.getValue()]))
                        );
                        parsedSomething = true;
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } else {
                    //TODO: Could not find applicable parser Error or Warning
                }
            }
        }

        return parsedSomething ? newInstance : null;
    }

    /*
    * Cache for CSV header mapper.
    * */
    private HashMap<Integer, Integer> headerMapper = null;

    /*
    * Required to be called **before** using the reading a CSV line by line.
    * */
    public void setHeader(String str) {
        this.headerMapper = genHashMapForMapping(str.split(SEPERATOR));
    }

    /*
    * Generates HashMap that map headers to fields.
    * Key = FieldIndex
    * Value = CellIndex
    * */
    private HashMap<Integer, Integer> genHashMapForMapping(String[] headers) {
        HashMap<Integer, Integer> indexMap = new HashMap<>();
        for (int i = 0; i < headers.length; i++) {
            for (int j = 0; j < fields.length; j++) {
                if (fields[j].getName().equals(headers[i])) {
                    indexMap.put(j, i);
                }
            }
        }
        return indexMap;
    }

    /*
    * Creates instance of the GenericType.
    * */
    private T instanceOfGenericType() {
        try {
            return genericType.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException
                | InvocationTargetException | NoSuchMethodException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}