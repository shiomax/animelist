package animelist.io.csv;

public interface ICellParser<T> {

    Class getGenericClass();

    boolean isValidFor(Class subject);

    boolean isValidFor(Object obj);

    T parse(String str);

    String toCsvCell(T obj);
}
