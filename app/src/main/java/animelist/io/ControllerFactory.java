package animelist.io;

public final class ControllerFactory {

    private static DbController dbController = null;

    public static IDataAccess getDefaultController() {
        return dbController == null ? (dbController = new DbController()) : dbController;
    }

    private static IDataAccessSources sourcesController = null;

    public static IDataAccessSources getSourcesController() {
        return sourcesController == null ? (sourcesController = new DbSourcesController()) : sourcesController;
    }
}
