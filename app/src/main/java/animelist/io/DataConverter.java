package animelist.io;

import animelist.io.csv.GenericCsvParser;
import animelist.types.Anime;
import animelist.types.AnimeSource;
import animelist.types.AnimeSourceEntity;
import animelist.types.SourceType;
import fxui.AppStart;
import fxui.GlobalUiCollection;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.stage.Modality;
import org.controlsfx.dialog.ProgressDialog;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

/*
 * This Controller is to export and import the library into various formats. Sort of backup and restore functions.
 * But, really CSV is the only option right now and the, I call it 'legacy' CSV format, i used in previous versions,
 * is available for import.
 *
 * Not really planning to add more options than just CSV anytime soon, but maybe some time. So one might argue it´s
 * named wrong for the time being.
 * */
public final class DataConverter {

    private static class importService extends Service<Integer> {

        private File file;
        private boolean importArtwork, importSources, importGlobalSources;
        private boolean legacyCSV;
        private String header;

        //The old CSV file didn't have a CSV header, however the Generic Csv Parser i made needs one to map fields
        private static final String CSV_HEADER = "name;secondName;seen;rating;source;sourceType;notes;dateAdded;lastEdit;contentType;description;audioType\n";

        //legacy import stuff
        private static final String CSV_SOURCE_TYPE_HTTP_LINK = "stream";
        private static final int CSV_INDEX_HTTP_LINK = 4;
        private static final int CSV_INDEX_HTTP_LINK_TYPE = 5;

        public importService(File file, boolean legacyHeader, boolean importArtwork, boolean importSources, boolean importGlobalSources) {
            this.file = file;
            this.header = legacyHeader ? CSV_HEADER : null;
            this.importGlobalSources = importGlobalSources;
            this.importSources = importSources;
            this.importArtwork = importArtwork;
            this.legacyCSV = legacyHeader;
        }

        /*
         * Counts file size in lines. Kinda makes it a little slower, but in return it´s possible to make accurate progress bars.
         * And it does not take *THAT* long. The whole generic parsing thing is a h3ll of a lot slower.
         * */
        public int count(String filename) throws IOException {
            InputStream is = new BufferedInputStream(new FileInputStream(filename));
            try {
                byte[] c = new byte[1024];
                int count = 0;
                int readChars = 0;
                boolean endsWithoutNewLine = false;
                while ((readChars = is.read(c)) != -1) {
                    for (int i = 0; i < readChars; ++i) {
                        if (c[i] == '\n')
                            ++count;
                    }
                    endsWithoutNewLine = (c[readChars - 1] != '\n');
                }
                if (endsWithoutNewLine) {
                    ++count;
                }
                return count;
            } finally {
                is.close();
            }
        }

        @Override
        protected Task<Integer> createTask() {
            return new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {

                    int max = count(file.getPath()) - 1;

                    int linesImported = 0;
                    updateProgress(0, max);

                    //Date Time formatter for image names
                    final DateTimeFormatter dateTimeFormatterForPaths = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

                    var imgPath = Paths.get(String.format("%s\\images", file.getParent()));
                    var findImages = Files.exists(imgPath); //if it does not exist no point in searching

                    //creating csvParser
                    GenericCsvParser<Anime> csvParser = new GenericCsvParser<>(Anime.class);
                    GenericCsvParser<AnimeSourceEntity> csvParserSource = new GenericCsvParser<>(AnimeSourceEntity.class);

                    //hashmap to map ids in csv to ids in db for mapping sources that refer to the animeid
                    HashMap<Long, Long> hashMapIdMapping = new HashMap<>();

                    var dataController = ControllerFactory.getDefaultController();
                    var dbSources = ControllerFactory.getSourcesController();

                    try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()))) {

                        if (header == null)
                            header = bufferedReader.readLine();

                        //setting header necessary for mapping
                        csvParser.setHeader(header);

                        String line;

                        while ((line = bufferedReader.readLine()) != null) {
                            var anime = csvParser.parseCsvLine(line);
                            Long id = null;
                            Long oldId = anime.getId();

                            //anime could be read
                            String path = null;
                            //if image Folder does not exist there is no point
                            if (importArtwork && findImages) {
                                if (legacyCSV) {
                                    //assemble legacy image path
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < anime.getName().length(); i++) {
                                        char c = anime.getName().charAt(i);
                                        if (Character.isAlphabetic(c) || Character.isDigit(c)) {
                                            stringBuilder.append(c);
                                        }
                                    }
                                    path = String.format("%s/%s%s.jpg", imgPath, stringBuilder.toString(), anime.getDateAdded().format(dateTimeFormatterForPaths));
                                } else {
                                    if (anime.getArtworkPath() != null && !anime.getArtworkPath().isEmpty())
                                        path = String.format("%s/%s", file.getParent(), anime.getArtworkPath());
                                }
                                if (path != null && Files.exists(Paths.get(path))) {
                                    anime.setArtworkPath(null);
                                    anime.setId(0);
                                    id = dataController.addAnime(anime, new File(path));
                                }
                                //TODO: add non-relative paths (currently this is not possible if not manually punshed into the db)
                                else {
                                    id = dataController.addAnime(anime);
                                }
                            } else {
                                id = dataController.addAnime(anime);
                            }

                            if (legacyCSV && id != null) {
                                var split = line.split(";");
                                var url = split[CSV_INDEX_HTTP_LINK];
                                if (split[CSV_INDEX_HTTP_LINK_TYPE].equals(CSV_SOURCE_TYPE_HTTP_LINK) && url != null && !url.isEmpty()) {
                                    var animeSource = new AnimeSource(url, null, SourceType.HttpLink);
                                    animeSource.setAnimeId(id);
                                    animeSource.setSourceName(url);
                                    dbSources.putSource(animeSource);
                                }
                            }

                            hashMapIdMapping.put(oldId, id);

                            updateProgress(++linesImported, max);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //importing sources
                    if (importGlobalSources || importSources) {

                        var pathSources = String.format("%s\\sources.csv", file.getParent());

                        if (Files.exists(Paths.get(pathSources))) {
                            max = count(pathSources) - 1;
                            int sourcesImported = 0;

                            updateProgress(sourcesImported, max);

                            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathSources))) {

                                String line = bufferedReader.readLine();

                                if(line != null) {
                                    csvParserSource.setHeader(line);

                                    while ((line = bufferedReader.readLine()) != null) {
                                        AnimeSourceEntity animeSourceEntity = csvParserSource.parseCsvLine(line);

                                        //add global source
                                        if(animeSourceEntity.getAnimeId() == null || animeSourceEntity.getAnimeId() <= 0) {
                                            animeSourceEntity.setAnimeId((long)-1);
                                            dbSources.putSource(animeSourceEntity);
                                        } else { //add specific source
                                            Long newId = hashMapIdMapping.get(animeSourceEntity.getAnimeId());
                                            if(newId != null) {
                                                animeSourceEntity.setAnimeId(newId);
                                                animeSourceEntity.setValidFor(null);
                                                dbSources.putSource(animeSourceEntity);
                                            }
                                        }

                                        updateProgress(++sourcesImported, max);
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    return linesImported;
                }
            };
        }
    }

    /*
     * Imports a csv file
     * */
    public static final void importCsvFile(File file, boolean legacyHeader, boolean importArtwork, boolean importSources, boolean importGlobalSources) {

        //TODO: import sources and global sources

        var service = new importService(file, legacyHeader, importArtwork, importSources, importGlobalSources);

        ProgressDialog progDiag = new ProgressDialog(service);
        progDiag.setTitle("AnimeList - Import");
        progDiag.initOwner(AppStart.getPrimaryStage());
        progDiag.setHeaderText(legacyHeader ? "Importing CSV (legacy)" : "Importing CSV");
        progDiag.setContentText("This might take a while.");
        progDiag.initModality(Modality.WINDOW_MODAL);
        service.start();

        service.setOnSucceeded(event -> {
            var value = (Integer) event.getSource().getValue();
            GlobalUiCollection.showNotification(AppStart.getPrimaryStage(), value + " entries imported sucessfully!");
        });
    }

    /*
     * Export Service for export to work in the background.
     * */
    private static class exportService extends Service {

        private List<Anime> list;
        private LocalDateTime localDateTime;
        private boolean includeImages, includeSources, includeGlobalSources;

        public exportService(List<Anime> list, LocalDateTime localDateTime, boolean includeImages, boolean includeSources, boolean includeGlobalSources) {
            this.list = list;
            this.localDateTime = localDateTime;
            this.includeImages = includeImages;
            this.includeSources = includeSources;
            this.includeGlobalSources = includeGlobalSources;
        }

        @Override
        protected Task createTask() {
            return new Task() {
                @Override
                protected Object call() throws Exception {

                    final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
                    int maxProgress = list.size();
                    int progress = 0;

                    var dbSources = ControllerFactory.getSourcesController();

                    updateProgress(progress, maxProgress);

                    if (!Files.exists(Paths.get("exports")))
                        Files.createDirectory(Paths.get("exports"));

                    Path exportFolder = Paths.get(String.format("exports/%s", localDateTime.format(dateTimeFormatter)));
                    Path imagesFolder = Paths.get(String.format("exports/%s/images", localDateTime.format(dateTimeFormatter)));

                    if (!Files.exists(exportFolder))
                        Files.createDirectory(exportFolder);
                    else {
                        throw new FileAlreadyExistsException("Directory " + exportFolder.toString() + " already exists. Can´t create.");
                    }

                    if (includeImages && !Files.exists(imagesFolder))
                        Files.createDirectory(imagesFolder);

                    var csvParser = new GenericCsvParser<>(Anime.class);
                    var csvParserSources = new GenericCsvParser<>(AnimeSourceEntity.class);

                    //NOTE: Files methods all use UTF-8 by default. The export will be UTF8 encoded.
                    try (var writer = Files.newBufferedWriter(Paths.get(exportFolder.toString() + "/animelist.csv"));
                         var sourceWriter = Files.newBufferedWriter(Paths.get(exportFolder.toString() + "/sources.csv"))) {

                        if (includeSources || includeGlobalSources) {
                            sourceWriter.write(csvParserSources.parseCsvHeader());
                            sourceWriter.write("\n");
                        }

                        if (includeGlobalSources) {
                            var globalSources = dbSources.getAllGlobalSources();
                            maxProgress += globalSources.size();

                            updateProgress(progress, maxProgress);

                            for (var s : globalSources) {
                                var line = csvParserSources.convertToCsvLine(s);
                                sourceWriter.write(line);
                                sourceWriter.write("\n");
                                updateProgress(++progress, maxProgress);
                            }
                        }

                        writer.write(csvParser.parseCsvHeader());
                        writer.write("\n");

                        for (var cur : list) {
                            var line = csvParser.convertToCsvLine(cur);
                            writer.write(line);
                            writer.write("\n");

                            //exporting images
                            if (includeImages && cur.getArtworkPath() != null && !cur.getArtworkPath().isEmpty() && Files.exists(Paths.get(cur.getArtworkPath()))) {
                                var curArtworkPath = Paths.get(cur.getArtworkPath());
                                var strPath = imagesFolder.toString() + "/" + curArtworkPath.getFileName().toString();
                                Files.copy(curArtworkPath, Paths.get(strPath));
                            }

                            //exporting sources
                            if (includeSources) {
                                var sources = dbSources.getSources(cur.getId());

                                if (sources != null) {
                                    for (var s : sources) {
                                        var str = csvParserSources.convertToCsvLine(s);
                                        if (str != null && !str.isEmpty()) {
                                            try {
                                                sourceWriter.write(str);
                                                sourceWriter.write("\n");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }
                            updateProgress(++progress, maxProgress);
                        }
                    } catch (IOException e) {
                        var message = e.getMessage();
                        e.printStackTrace();
                    }

                    return null;
                }
            };
        }
    }

    /*
     * Exports a List of Animes to a csv File.
     * */
    public static final void exportToCsv(List<Anime> list, LocalDateTime localDateTime, boolean includeImages, boolean includeSources, boolean includeGlobalSources) {
        var service = new exportService(list, localDateTime, includeImages, includeSources, includeGlobalSources);

        ProgressDialog progDiag = new ProgressDialog(service);
        progDiag.setTitle("AnimeList - Export");
        progDiag.initOwner(AppStart.getPrimaryStage());
        progDiag.setHeaderText("Exporting CSV");
        progDiag.setContentText("This might take a while.");
        progDiag.initModality(Modality.WINDOW_MODAL);
        service.start();

        service.setOnSucceeded(event -> {
            GlobalUiCollection.showNotification(AppStart.getPrimaryStage(), "Library exported sucessfully to export Folder.");
        });
    }
}