package animelist.io;

import animelist.types.AnimeSource;
import animelist.types.AnimeSourceEntity;
import animelist.types.ContentType;
import org.pmw.tinylog.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class DbSourcesController implements IDataAccessSources{

    private static final String CONNECTION_STRING = "jdbc:sqlite:sources.sqlite";

    static {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            createTables(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //region Table def

    private static final String TABLE_SOURCES = "sources";

    private enum SOURCES {
        id, //primary key
        sourceName, //name of the source if null it´s the url
        validFor, //valid for ContentTypes, null if any, or a specific entry
        sourceValue, //value of the source, can be Folder path or url
        sourceType, //specifies the type of sourceValue
        animeId //valid for specific anime, null if global
    }

    //endregion

    //region interface implementation

    @Override
    public List<AnimeSource> getSources(long animeId) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            return getWithId(con, animeId, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new LinkedList<>();
    }

    @Override
    public List<AnimeSource> getGlobalSources(ContentType contentType) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            return getWithId(con, -1, contentType);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new LinkedList<>();
    }

    @Override
    public List<AnimeSource> getAllGlobalSources() {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            return getAllGlobal(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new LinkedList<>();
    }

    @Override
    public AnimeSourceEntity putSource(AnimeSourceEntity animeSource) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            animeSource = insertSource(con, animeSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return animeSource;
    }

    @Override
    public void updateSource(AnimeSource animeSource) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            updateSource(con, animeSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteSource(long sourceId) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            deleteSource(con, sourceId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteWithAnimeId(long animeId) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            deleteWithAnimeId(con, animeId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAll(boolean keepGlobal) {
        if(keepGlobal) {
            try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
                deleteAllSpecificSources(con);
            } catch (SQLException e) { }
        } else {
            try {
                Files.delete(Paths.get("sources.sqlite"));
                try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
                    createTables(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) { }
        }
    }

    //endregion

    //region SQL-Wasteland

    private static boolean deleteSource(Connection con, long id) {
        String sql = "DELETE FROM " + TABLE_SOURCES + " WHERE " + SOURCES.id.toString() + " = " + id;

        try {
            long del = con.prepareStatement(sql).executeUpdate();
            return del > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    private static long deleteWithAnimeId(Connection con, long id) {
        String sql = "DELETE FROM " + TABLE_SOURCES + " WHERE " + SOURCES.animeId.toString() + " = " + id;

        try {
            return con.prepareStatement(sql).executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private static long deleteAllSpecificSources(Connection con) {
        String sql = "DELETE FROM " + TABLE_SOURCES + " WHERE " + SOURCES.animeId.toString() + " = -1";

        try {
            return con.prepareStatement(sql).executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private static List<AnimeSource> getAllGlobal(Connection con) {
        return getWithId(con, -1, null);
    }

    private static List<AnimeSource> getWithId(Connection con, long animeId, ContentType contentType) {
        String sql = "SELECT * FROM " + TABLE_SOURCES + " WHERE " + SOURCES.animeId + " = " + animeId;

        try {
            var query = con.prepareStatement(sql);
            var result = query.executeQuery();

            List<AnimeSource> list = new LinkedList<>();

            while(result.next()) {
                String sourceType = result.getString(SOURCES.sourceType.toString());
                String sourceValue = result.getString(SOURCES.sourceValue.toString());
                String validFor = result.getString(SOURCES.validFor.toString());
                Integer id = result.getInt(SOURCES.id.toString());
                String sourceName = result.getString(SOURCES.sourceName.toString());
                Long idAnime = result.getLong(SOURCES.animeId.toString());

                var cur = new AnimeSource(id, sourceValue, validFor, sourceName, sourceType);
                cur.setAnimeId(idAnime);

                if(contentType == null || cur.isValidFor(contentType))
                    list.add(cur);
            }

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static boolean updateSource(Connection con, AnimeSource animeSource) {
        String sql = "UPDATE " + TABLE_SOURCES + " SET "
                + SOURCES.validFor + " = ?, "
                + SOURCES.sourceName + " = ?, "
                + SOURCES.animeId + " = ?, "
                + SOURCES.sourceType + " = ?, "
                + SOURCES.sourceValue + " = ? "
                + "WHERE " + SOURCES.id + " = ?";

        try {
            var query = con.prepareStatement(sql);

            query.setString(1, animeSource.getValidFor());
            query.setString(2, animeSource.getSourceName());
            query.setLong(3, animeSource.getAnimeId() == null ? -1 : animeSource.getAnimeId());
            query.setString(4, animeSource.getSourceType().toString());
            query.setString(5, animeSource.getSourceValue());

            query.setInt(6, animeSource.getId());

            var affectedRows = query.executeUpdate();

            return affectedRows >= 1;
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.error("[DbSources] Panic, update statement failed! ExceptionMsg: " + e.getMessage());
        }

        return false;
    }

    private static AnimeSourceEntity insertSource(Connection con, AnimeSourceEntity animeSource) {

        String sql = "INSERT INTO " + TABLE_SOURCES + " (" +
                SOURCES.validFor + ", " +
                SOURCES.sourceValue + ", " +
                SOURCES.sourceType + ", " +
                SOURCES.animeId + ", " +
                SOURCES.sourceName + ") VALUES(?,?,?,?,?)";

        try {
            var query = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            query.setString(1, animeSource.getValidFor());
            query.setString(2, animeSource.getSourceValue());
            query.setString(3, animeSource.getSourceType().toString());
            query.setLong(4, animeSource.getAnimeId() == null ? -1 : animeSource.getAnimeId());
            query.setString(5, animeSource.getSourceName());

            var affectedRows = query.executeUpdate();

            if(affectedRows == 0) {
                Logger.error("[DbSources] Panic Insert Failed! Zero rows affected by insert statement.");
            }

            try (var keys = query.getGeneratedKeys()) {
                if (keys.next()) {
                    animeSource.setId(keys.getInt(1));
                    return animeSource;
                }
                else {
                    Logger.error("[DbSources] Panic, no key generated by sqlite for insert statement!");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.error("[DbSources] Panic, insert failed. Exception msg: " + e.getMessage());
        }

        return animeSource;
    }

    /*
    * Creates the tables if they do not yet exist
    * */
    private static void createTables(Connection con) {

        String createSources = "CREATE TABLE IF NOT EXISTS " + TABLE_SOURCES + " (\n"
                + SOURCES.id + " INTEGER PRIMARY KEY,\n"
                + SOURCES.sourceName + " TEXT,\n"
                + SOURCES.validFor + " TEXT,\n"
                + SOURCES.sourceValue + " TEXT,\n"
                + SOURCES.sourceType + " INTEGER,\n"
                + SOURCES.animeId + " INTEGER\n"
                + ");";

        try {
            con.createStatement().execute(createSources);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //endregion
}
