package animelist.io;

import animelist.GlobalStore;
import animelist.types.Anime;
import animelist.types.AudioType;
import animelist.types.ContentType;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.image.Image;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.*;

/*
* DbController for a SqlLiteDb, uses jdbc driver to read and write.
* I don´t think there is a JPA library for SqlLite, but either way it´s
* not that big of a deal, cause there are not a lot of Tables.
*
* Just gotta cross fingers for no weird typos.
* */
public class DbController implements IDataAccess {

    private static final String CONNECTION_STRING = "jdbc:sqlite:animelist.sqlite";
    private static final String IMAGE_FOLDER = "images";

    private ObservableList<Anime> observableList = null;
    private SortedList<Anime> sortedList = null;
    private FilteredList<Anime> filteredList = null;

    private SimpleIntegerProperty librarySize = new SimpleIntegerProperty(0);

    @Override
    public ObservableList<Anime> getAll() {
        if (observableList == null)
            initialLoad();
        return observableList;
    }

    @Override
    public SortedList<Anime> getAllSorted() {
        if (sortedList == null)
            initialLoad();
        return sortedList;
    }

    @Override
    public FilteredList<Anime> getAllFiltered() {
        if (filteredList == null)
            initialLoad();
        return filteredList;
    }

    @Override
    public SimpleIntegerProperty librarySize() {
        return this.librarySize;
    }

    @Override
    public Anime getById(long id) {
        var result = observableList.stream().filter(val -> val.getId() == id).findAny();

        return result.orElse(null);
    }

    @Override
    public long addAnime(Anime anime) {
        return addAnime(anime, null);
    }

    @Override
    public long addAnime(Anime anime, File artwork) {
        Anime entity = null;

        try (var con = DriverManager.getConnection(CONNECTION_STRING)){

            entity = insert(con, anime);
            observableList.add(entity);
            this.librarySize.set(this.librarySize.getValue() + 1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(entity != null && artwork != null) {
            this.updateAnimeArtworkFromFile(anime, artwork);
        }

        return entity == null ? 0 : entity.getId();
    }

    @Override
    public Anime updateAnime(Anime anime) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {

            var sucess = this.update(con, anime);

            if(sucess) {
                observableList.forEach(val -> {
                    if(val.getId() == anime.getId())
                        val.copyProperties(anime);
                });
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void deleteAnime(Anime anime) {
        try (var con = DriverManager.getConnection(CONNECTION_STRING)){
            delete(con, anime.getId());
            var dbSources = ControllerFactory.getSourcesController();
            dbSources.deleteWithAnimeId(anime.getId());
            observableList.remove(anime);
            this.librarySize.set(this.librarySize.getValue() - 1);
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO: TOTAL PANIC COULD NOT DELETE
        }
    }

    /*
    * Removes anime with id as param.
    * */
    @Override
    public void deleteAnime(long id) {
        var toRemove = observableList.stream().filter(val -> val.getId() == id).findAny();

        if(toRemove.isPresent()) {
            updateAnimeArtworkFromURL(toRemove.get(), null);
            deleteAnime(toRemove.get());
        } else {
            //TODO: TOTAL PANIC COULD NOT DELETE
        }
    }

    @Override
    public ImageResponse getArtwork(Anime anime) {
        String path = anime.getArtworkPath();

        if(path != null && !path.isEmpty() && Files.exists(Paths.get(path))) {
            var img = new Image(Paths.get(anime.getArtworkPath()).toUri().toString());
            return new ImageResponse(img, path, false);
        }

        return new ImageResponse(GlobalStore.NO_IMAGE_IMAGE, path, true);
    }

    @Override
    public ImageResponse getArtwork(long id) {
        var anime = observableList.stream().filter(val -> val.getId() == id).findAny();

        return anime.isPresent() ? getArtwork(anime.get()) : new ImageResponse(GlobalStore.NO_IMAGE_IMAGE, null, true);
    }

    @Override
    public void updateAnimeArtworkFromFile(Anime anime, File artwork) {
        try {
            this.updateAnimeArtworkFromURL(anime, artwork == null ? null : artwork.toURI().toURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAnimeArtworkFromURL(Anime anime, URL url) {
        //Images Folder must exist
        if(!Files.exists(Paths.get(IMAGE_FOLDER))) {
            try {
                Files.createDirectory(Paths.get(IMAGE_FOLDER));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //remove old artwork if exist
        if(anime.getArtworkPath() != null && !anime.getArtworkPath().isEmpty() && Files.exists(Paths.get(anime.getArtworkPath()))) {
            try {
                Files.delete(Paths.get(anime.getArtworkPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(url == null) {
            anime.setArtworkPath(null);
            updateArtworkPath(anime);
            return;
        }

        String urlPath = url.getPath();
        String extension = null;
        if(urlPath.contains(".")) {
            extension = urlPath.substring(urlPath.lastIndexOf("."));
        }

        //TODO: JavaFx Image only supports BMP, GIF JPEG and PNG check for file format, for now it just "trusts the user"
        var newPath = String.format("%s/%s%s", IMAGE_FOLDER, anime.getId(), extension == null ? "" : extension);

        try {
            ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(newPath);
            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

            //update path
            anime.setArtworkPath(newPath);
            updateArtworkPath(anime);

            fileOutputStream.close();
            readableByteChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    * Helper method for updateAnimeArtworkFromURL(Anime anime, URL url)
    * */
    private void updateArtworkPath(Anime anime) {
        try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
            boolean sucess = this.updateArtworkPath(con, anime); //udpate table
            if(sucess) {
                //update observable list
                observableList.stream().forEach(val -> {
                    if (val.getId() == anime.getId())
                        anime.setArtworkPath(anime.getArtworkPath());
                });
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAnimeArtworkFromStrURL(Anime anime, String url) {
        try {
            updateAnimeArtworkFromURL(anime, (url == null || url.isEmpty()) ? null : new URL(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean resetLirary(boolean keepGlobalSources) {
        this.observableList.clear();

        var delted = deleteDbFileAndImages();
        var dbSources = ControllerFactory.getSourcesController();
        dbSources.deleteAll(keepGlobalSources);

        //create table again
        if(delted) {
            try(var con = DriverManager.getConnection(CONNECTION_STRING)) {
                createTable(con);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            this.librarySize.set(0);
        }

        return delted;
    }

    private boolean deleteDbFileAndImages() {
        try {
            Files.delete(Paths.get("animelist.sqlite"));
        } catch (IOException e) {
            if(Files.exists(Paths.get("animelist.sqlite")))
                return false;
        }

        Path directory = Paths.get("images");
        try {
            Files.walkFileTree(directory, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) { }
        return true;
    }

    private void initialLoad() {
        this.observableList = FXCollections.observableArrayList();

        try (var con = DriverManager.getConnection(CONNECTION_STRING)){

            createTable(con);
            appendToList(selectAll(con));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.filteredList = new FilteredList<>(observableList);
        this.sortedList = new SortedList<>(filteredList);
        this.librarySize.set(observableList.size());
    }

    //region SQL-Wasteland

    private static final String TABLE_NAME = "animes";

    /*
     * SQL-Column-Names
     * */

    /*
    * Some collection of SQL columns to avoid stupid mistypes...
    * */
    private static final class SQL_COLUMN {
        static final String ID = "id";
        static final String NAME = "name";
        static final String SECOND_NAME = "secondName";
        static final String NOTES = "notes";
        static final String DESCRIPTION = "description";
        static final String ARTWORK_PATH = "artworkPath";
        static final String CONTENT_TYPE = "contentType";
        static final String DATE_ADDED = "dateAdded";
        static final String DATE_LAST_EDIT = "dateLastAdded";
        static final String SEEN = "watched";
        static final String RATING = "rating";
        static final String AUDIO_TYPE = "audioType";
        static final String EPISODE_COUNT = "episodeCount";
        static final String CONTINUE_WATCHING = "continueWatching";
    }

    /*
     * Creates Animes Table if it does not exist
     * */
    private void createTable(Connection con) {

        final String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (\n"
                + SQL_COLUMN.ID + " INTEGER PRIMARY KEY,\n"
                + SQL_COLUMN.NAME + " TEXT NOT NULL,\n"
                + SQL_COLUMN.SECOND_NAME + " TEXT,\n"
                + SQL_COLUMN.NOTES + " TEXT,\n"
                + SQL_COLUMN.DESCRIPTION + " TEXT,\n"
                + SQL_COLUMN.ARTWORK_PATH + " TEXT,\n"
                + SQL_COLUMN.CONTENT_TYPE + " TEXT,\n"
                + SQL_COLUMN.DATE_ADDED + " TIMESTAMP NOT NULL,\n"
                + SQL_COLUMN.DATE_LAST_EDIT + " TIMESTAMP NOT NULL,\n"
                + SQL_COLUMN.SEEN + " BOOLEAN,\n"
                + SQL_COLUMN.RATING + " REAL,\n"
                + SQL_COLUMN.AUDIO_TYPE + " TEXT,\n"
                + SQL_COLUMN.EPISODE_COUNT + " INTEGER,\n"
                + SQL_COLUMN.CONTINUE_WATCHING + " INTEGER\n"
                + ");";

        try {
            con.createStatement().execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
     * Inserts anime into table
     * */
    private Anime insert(Connection con, Anime anime) {

        final String sql = "INSERT INTO " + TABLE_NAME + " ("
                + SQL_COLUMN.NAME + ","
                + SQL_COLUMN.SECOND_NAME + ","
                + SQL_COLUMN.NOTES + ","
                + SQL_COLUMN.DESCRIPTION + ","
                + SQL_COLUMN.ARTWORK_PATH + ","
                + SQL_COLUMN.CONTENT_TYPE + ","
                + SQL_COLUMN.DATE_ADDED + ","
                + SQL_COLUMN.DATE_LAST_EDIT + ","
                + SQL_COLUMN.SEEN + ","
                + SQL_COLUMN.RATING + ","
                + SQL_COLUMN.AUDIO_TYPE + ","
                + SQL_COLUMN.EPISODE_COUNT + ","
                + SQL_COLUMN.CONTINUE_WATCHING + ") "
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {
            var query = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            int i = 1;

            query.setString(i++, anime.getName()); //SQL_COLUMN.NAME
            query.setString(i++, anime.getSecondName()); //SQL_COLUMN.SECOND_NAME
            query.setString(i++, anime.getNotes()); //SQL_COLUMN.NOTES
            query.setString(i++, anime.getDescription()); //SQL_COLUMN.DESCRIPTION
            query.setString(i++, anime.getArtworkPath()); //SQL_COLUMN.ARTWORK_PATH
            query.setString(i++, anime.getContentType().toString()); //SQL_COLUMN.CONTENT_TYPE
            query.setTimestamp(i++, Timestamp.valueOf(anime.getDateAdded())); //SQL_COLUMN.DATE_ADDED
            query.setTimestamp(i++, Timestamp.valueOf(anime.getLastEdit())); //SQL_COLUMN_DATE_LAST_EDIT
            query.setBoolean(i++, anime.getSeen()); //SQL_COLUMN_WATCHED
            query.setDouble(i++, anime.getRating()); //SQL_COLUMN_RATING
            query.setString(i++, anime.getAudioType().toString()); //SQL_COLUMN_AUDIO_TYPE
            query.setInt(i++, anime.getEpisodeCount()); //SQL_COLUMN_EPISODE_COUNT
            query.setInt(i++, anime.getContinueWatching()); //SQL_COLUMN_CONTINUE_WATCHING

            var affectedRows = query.executeUpdate();

            if (affectedRows == 0) {
                Logger.error("Panic Insert Failed! Zero rows affected by insert statement.");
            }

            try (var keys = query.getGeneratedKeys()) {
                if (keys.next()) {
                    anime.setId(keys.getLong(1));
                    return anime;
                }
                else {
                    Logger.error("Panic, no key generated by sqlite for insert statement!");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.error("Panic, insert failed. Exception msg: " + e.getMessage());
        }

        return null;
    }

    /*
     * Selects everything
     * */
    private ResultSet selectAll(Connection con) {

        final String sql = "SELECT * FROM " + TABLE_NAME;

        try {
            var result = con.createStatement().executeQuery(sql);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /*
    * Appends a ResultSet to the ObservableList
    * */
    private void appendToList(ResultSet resultSet) {
        try {
            while (resultSet.next()) {
                Anime anime = new Anime(
                        resultSet.getInt(SQL_COLUMN.ID),
                        resultSet.getString(SQL_COLUMN.NAME),
                        resultSet.getString(SQL_COLUMN.SECOND_NAME),
                        resultSet.getBoolean(SQL_COLUMN.SEEN),
                        resultSet.getDouble(SQL_COLUMN.RATING),
                        resultSet.getString(SQL_COLUMN.NOTES),
                        resultSet.getTimestamp(SQL_COLUMN.DATE_ADDED).toLocalDateTime(),
                        resultSet.getTimestamp(SQL_COLUMN.DATE_LAST_EDIT).toLocalDateTime(),
                        ContentType.valueOf(resultSet.getString(SQL_COLUMN.CONTENT_TYPE)),
                        resultSet.getString(SQL_COLUMN.DESCRIPTION),
                        AudioType.valueOf(resultSet.getString(SQL_COLUMN.AUDIO_TYPE)),
                        resultSet.getInt(SQL_COLUMN.EPISODE_COUNT),
                        resultSet.getInt(SQL_COLUMN.CONTINUE_WATCHING)
                );
                var str = resultSet.getString(SQL_COLUMN.ARTWORK_PATH);
                //TODO: Windows should not save \ into paths ever! Then this wont be needed.
                anime.setArtworkPath(str == null ? null : str.replace('\\', '/'));
                observableList.add(anime);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.error("Panic, failed to parse ResultSet! ExceptionMsg: " + e.getMessage());
        }
    }


    /*
    * Deletes row with id
    * */
    private boolean delete(Connection con, long id) {

        final String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + SQL_COLUMN.ID + " = ?";

        try {
            var query = con.prepareStatement(sql);
            query.setLong(1, id);

            int val = query.executeUpdate();
            return val >= 1;
        } catch (SQLException e) {
            Logger.error("Panic, delete failed. " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    /*
    * Updates an entire row
    * */
    private boolean update(Connection con, Anime anime) {

        final String sql = "UPDATE " + TABLE_NAME + " SET "
                + SQL_COLUMN.NAME + " = ?, "
                + SQL_COLUMN.SECOND_NAME + " = ?, "
                + SQL_COLUMN.NOTES + " = ?, "
                + SQL_COLUMN.DESCRIPTION + " = ?, "
                + SQL_COLUMN.ARTWORK_PATH + " = ?, "
                + SQL_COLUMN.CONTENT_TYPE + " = ?, "
                + SQL_COLUMN.DATE_ADDED + " = ?, "
                + SQL_COLUMN.DATE_LAST_EDIT + " = ?, "
                + SQL_COLUMN.SEEN + " = ?, "
                + SQL_COLUMN.RATING + " = ?, "
                + SQL_COLUMN.AUDIO_TYPE + " = ?, "
                + SQL_COLUMN.EPISODE_COUNT + " = ?, "
                + SQL_COLUMN.CONTINUE_WATCHING + " = ? "
                + " WHERE " + SQL_COLUMN.ID + " = ?";

        try {
            var query = con.prepareStatement(sql);

            query.setString(1, anime.getName()); //SQL_COLUMN.NAME
            query.setString(2, anime.getSecondName()); //SQL_COLUMN.SECOND_NAME
            query.setString(3, anime.getNotes()); //SQL_COLUMN.NOTES
            query.setString(4, anime.getDescription()); //SQL_COLUMN.DESCRIPTION
            query.setString(5, anime.getArtworkPath()); //SQL_COLUMN.ARTWORK_PATH
            query.setString(6, anime.getContentType().toString()); //SQL_COLUMN.CONTENT_TYPE
            query.setTimestamp(7, Timestamp.valueOf(anime.getDateAdded())); //SQL_COLUMN.DATE_ADDED
            query.setTimestamp(8, Timestamp.valueOf(anime.getLastEdit())); //SQL_COLUMN_DATE_LAST_EDIT
            query.setBoolean(9, anime.getSeen()); //SQL_COLUMN_WATCHED
            query.setDouble(10, anime.getRating()); //SQL_COLUMN_RATING
            query.setString(11, anime.getAudioType().toString()); //SQL_COLUMN_AUDIO_TYPE
            query.setInt(12, anime.getEpisodeCount()); //SQL_COLUMN_EPISODE_COUNT
            query.setInt(13, anime.getContinueWatching()); //SQL_COLUMN_CONTINUE_WATCHING

            query.setLong(14, anime.getId()); //SQL_COLUMN.ID for where clause

            var affectedRows = query.executeUpdate();

            return affectedRows >= 1;
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.error("Panic, update statement failed! ExceptionMsg: " + e.getMessage());
        }

        return false;
    }

    /*
    * Updates only the ArtworkPath column.
    * Sorta required as artworkPath is managed seperately
    * */
    private boolean updateArtworkPath(Connection con, Anime anime) {

        final String sql = "UPDATE " + TABLE_NAME + " SET "
                + SQL_COLUMN.ARTWORK_PATH + " = ? "
                + "WHERE " + SQL_COLUMN.ID + " = " + anime.getId();

        try {
            var query = con.prepareStatement(sql);

            query.setString(1, anime.getArtworkPath());

            var affectedRows = query.executeUpdate();

            return affectedRows >= 1;
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.error("Panic, artwork update statement failed! ExceptionMsg: " + e.getMessage());
        }

        return false;
    }

    //endregion
}