package animelist;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.format.DateTimeFormatter;

/*
* Just some class for storing values that are used threwout the entire project to avoid duplication
* and have one point for changes.
* Config file might be better for some of these things, but this works fine. (for now anyways)
* */
public class GlobalStore {
    public static final Image IMAGE_ANIME_SEEN = new Image("images/musicpackage/checked.png");
    public static final int IMAGE_ANIME_SEEN_HEIGHT = 16;
    public static final int IMAGE_ANIME_SEEN_WIDTH = 16;

    public static final Image IMAGE_ANIME_NOT_SEEN = new Image("images/musicpackage/minus.png");
    public static final int IMAGE_ANIME_NOT_SEEN_HEIGHT = 16;
    public static final int IMAGE_ANIME_NOT_SEEN_WIDTH = 16;

    public static final Image IMAGE_PLAY = new Image("images/musicpackage/play-button.png");
    public static final int IMAGE_PLAY_HEIGHT = 16;
    public static final int IMAGE_PLAY_WIDTH = 16;

    public static final Image IMAGE_PAUSE = new Image("images/musicpackage/pause.png");
    public static final int IMAGE_PAUSE_HEIGHT = 16;
    public static final int IMAGE_PAUSE_WIDTH = 16;

    public static final String TITLE_MAIN = "AnimeList";
    public static final String TITLE_ADD_ANIME = "AnimeList - Add Anime";
    public static final String TITLE_EDIT_ANIME = "AnimeList - Edit Anime";
    public static final String TITLE_SETTINGS = "AnimeList - Settings";
    public static final String TITLE_MAL_API = "AnimeList - myanimelist.net-pluginapi";
    public static final String TITLE_ABOUT = "AnimeList - About";

    public static final Image NO_IMAGE_IMAGE = new Image("images/noimage.jpg");

    public static final DateTimeFormatter DATE_TIME_FORMATTER =  DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    private static String jarPath = null;

    static {

    }

    public static String getJarPath() {
        return jarPath;
    }

    public static void setJarPath(String jarPath) {
        GlobalStore.jarPath = jarPath;
    }

    public static ImageView getImageViewPlay() {
        return createView(IMAGE_PLAY, IMAGE_PLAY_HEIGHT, IMAGE_PLAY_WIDTH);
    }

    public static ImageView getImageViewPause() {
        return createView(IMAGE_PAUSE, IMAGE_PAUSE_HEIGHT, IMAGE_PAUSE_WIDTH);
    }

    public static ImageView getAnimeNotSeenImageView() {
        return createView(IMAGE_ANIME_NOT_SEEN, IMAGE_ANIME_NOT_SEEN_HEIGHT, IMAGE_ANIME_NOT_SEEN_WIDTH);
    }

    public static ImageView getAnimeSeenImageView() {
        return createView(IMAGE_ANIME_SEEN, IMAGE_ANIME_SEEN_HEIGHT, IMAGE_ANIME_SEEN_WIDTH);
    }

    private static ImageView createView(Image img, int hfit, int wfit) {
        var view = new ImageView(img);
        view.setFitHeight(hfit);
        view.setFitWidth(wfit);
        return view;
    }
}
