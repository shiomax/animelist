package animelist.types;

/*
* Base class of AnimeSource. Used to be able to use reflection when saving it to a CSV.
* Otherwise it would not be necessary, but it still is more pretty to have the Entities how they
* are saved in the db as simple POJOs regardless.
* */
public class AnimeSourceEntity {
    private Integer id;
    private Long animeId;
    private String sourceName, validFor, sourceValue;
    private SourceType sourceType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getAnimeId() {
        return animeId;
    }

    public void setAnimeId(Long animeId) {
        this.animeId = animeId;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getValidFor() {
        return validFor;
    }

    public void setValidFor(String validFor) {
        this.validFor = validFor;
    }

    public String getSourceValue() {
        return sourceValue;
    }

    public void setSourceValue(String sourceValue) {
        this.sourceValue = sourceValue;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
    }
}