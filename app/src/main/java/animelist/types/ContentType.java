package animelist.types;

public enum ContentType {
    Anime,
    AnimeMovie,
    Manga,
    Movie,
    Series,
    AnimeManga
}
