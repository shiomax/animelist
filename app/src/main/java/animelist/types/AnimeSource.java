package animelist.types;

import fxui.GlobalUiCollection;
import javafx.application.Platform;

import java.awt.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * GlobalSource is a feature that let´s you save Websites, that you often visit,
 * open them with any of the fields of your Animelist-entries as parameter(s).
 *
 * Add once, use with anything.
 *
 * Format for the URL string will be like examplesite.com/search/?q=%name%
 * */
public class AnimeSource extends AnimeSourceEntity {

    private static final char TOKEN_SYM = '%';

    //Fields of an anime for casting things like %name%
    private static Field[] fields = null;

    static {
        loadFields();
    }

    private List<urlPart> urlParts;

    //The Link can be used for those ContentTypes, **null if any**
    private List<ContentType> validForList;

    public AnimeSource() {

    }

    public AnimeSource(String urlString, List<ContentType> validFor, SourceType sourceType) {
        setTarget(urlString, validFor, sourceType);
    }

    public AnimeSource(String urlString, SourceType sourceType) {
        this(urlString, null, sourceType);
    }

    /*
     * for construction from db
     * */
    public AnimeSource(Integer id, String sourceValue, String validFor, String sourceName, String sourceType) {
        super.setId(id);
        super.setSourceName(sourceName);
        this.setTarget(sourceValue, revertValidForString(validFor), SourceType.valueOf(sourceType));
    }

    public List<ContentType> getValidForList() {
        return this.validForList;
    }

    public String getValue(Anime anime) {
        StringBuilder sb = new StringBuilder();
        for (var cur : urlParts)
            sb.append(cur.getValue(anime));
        return sb.toString();
    }

    public void setTarget(String urlString, List<ContentType> validFor, SourceType sourceType) {
        super.setSourceType(sourceType);
        super.setSourceValue(urlString);
        this.validForList = validFor;
        setValidForString();
        urlParts = new LinkedList<>();
        createUrlParts(urlString);
    }

    /*
    * Sets the ValidForString if a List was used for creating the instance
    * */
    private void setValidForString() {
        if (validForList == null || validForList.isEmpty()) {
            super.setValidFor(null);
        } else {
            super.setValidFor(
                    this.validForList.stream()
                            .map(ContentType::toString)
                            .collect(Collectors.joining(","))
            );
        }
    }

    //region simple-properties

    public String getDisplayName() {
        return super.getSourceName() == null || super.getSourceName().isEmpty() ?
                super.getSourceValue() : super.getSourceName();
    }

    //endregion

    private List<ContentType> revertValidForString(String str) {
        if (str == null || str.isEmpty())
            return null;

        var list = new LinkedList<ContentType>();

        for (var cur : str.split(",")) {
            list.add(ContentType.valueOf(cur));
        }

        return list.size() > 0 ? list : null;
    }

    /*
     * Opens the source in a browser or in a file explorer depending on it´s type
     * */
    public void openSource(Anime anime) {
        openSource(getValue(anime));
    }

    public void openSource(String urlString) {
        if (super.getSourceType() == SourceType.HttpLink && urlString != null) {
            if (urlString.startsWith("http") == false)
                urlString = "http://" + urlString;
            final String browseUrl = urlString;
            if(Desktop.isDesktopSupported()) {
                new Thread(() -> {
                    try {
                        Desktop.getDesktop().browse(new URI(browseUrl));
                    } catch (IOException | URISyntaxException e) {
                        Platform.runLater(() -> {
                            GlobalUiCollection.showError("IOException|URISyntaxexception", "Your URL is not formatted correctly.");
                        });
                    }
                }).start();

            }
        } else {
            //TODO: implement folder opening
        }
    }

    /*
     * Parses an url String, does not work for double %%name%, but could be argued that´s
     * user error anyways. Otherwise works fine.
     * */
    private void createUrlParts(String urlString) {

        StringBuilder buffer = new StringBuilder();
        StringBuilder token = new StringBuilder();
        boolean tokenMode = false;

        for (int i = 0; i < urlString.length(); i++) {
            char cur = urlString.charAt(i);

            //In token mode a field might be added
            if (tokenMode) {
                if (cur == TOKEN_SYM) {
                    String str = token.toString();
                    Field field = searchField(str);

                    if (field != null) {
                        if (buffer.length() > 0) {
                            this.urlParts.add(new urlPart(buffer.toString()));
                            buffer.setLength(0);
                        }
                        this.urlParts.add(new urlPart(field));
                    } else {
                        buffer.append(TOKEN_SYM + str);
                        if (i == str.length() - 1)
                            buffer.append(TOKEN_SYM);
                        else
                            i--;
                    }
                    token.setLength(0);
                    tokenMode = false;
                } else {
                    token.append(cur);
                }
            } else {
                if (cur == TOKEN_SYM)
                    tokenMode = true;
                else
                    buffer.append(cur);
            }
        }
        if (token.length() > 0) {
            buffer.append(TOKEN_SYM);
            buffer.append(token);
        }
        if (buffer.length() > 0)
            this.urlParts.add(new urlPart(buffer.toString()));
    }

    private Field searchField(String token) {
        for (var cur : fields) {
            if (cur.getName().equals(token))
                return cur;
        }
        return null;
    }

    /*
     * Loads the fields of Anime.class and sets them accessible
     * if not loaded already.
     * */
    private static void loadFields() {
        if (fields == null) {
            fields = Anime.class.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
            }
        }
    }

    public boolean isValidFor(ContentType contentType) {
        return this.validForList.contains(contentType);
    }

    //TODO: this should be handled by the constructor non-existent param = all
    public void setValidAll() {
        this.validForList = Arrays.asList(ContentType.values());
        this.setValidForString();
    }

    private static class urlPart {
        private String str = null;
        private Field field = null;

        public urlPart(String str) {
            this.str = str;
        }

        public urlPart(Field field) {
            this.field = field;
        }

        public String getValue(Anime anime) {
            if (str != null)
                return str;
            try {
                try {
                    return URLEncoder.encode(field.get(anime).toString(), StandardCharsets.UTF_8.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return "";
        }
    }
}
