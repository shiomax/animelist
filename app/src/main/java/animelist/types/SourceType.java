package animelist.types;

public enum SourceType {
    HttpLink(0),
    Folder(1);

    private int id;

    SourceType(int id) {
        this.id = id;
    }

    public int getValue() {
        return this.id;
    }

    public SourceType getFromId(int id) {
        switch (id) {
            case 0:
                return SourceType.HttpLink;
            case 1:
                return SourceType.Folder;
        }
        return null;
    }
}
