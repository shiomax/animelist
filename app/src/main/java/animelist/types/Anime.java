package animelist.types;

import java.time.LocalDateTime;

public class Anime {

    private String name, secondName, notes, description, artworkPath;
    private ContentType contentType;
    private LocalDateTime dateAdded, lastEdit;
    private Boolean seen;
    private double rating;
    private AudioType audioType;
    private long id;
    private int episodeCount, continueWatching;

    //region constructors

    public Anime(long id, String name, String secondName, boolean watched, Double rating, String notes, LocalDateTime dateAdded,
                 LocalDateTime lastEdit, ContentType contentType, String description,
                 AudioType audioType, int episodeCount, int continueWatching) {
        this.name = name;
        this.secondName = secondName;
        this.seen = watched;
        this.rating = rating;
        this.notes = notes;
        this.dateAdded = dateAdded;
        this.lastEdit = lastEdit;
        this.contentType = contentType;
        this.description = description;
        this.audioType = audioType;
        this.id = id;
        this.episodeCount = episodeCount;
        this.continueWatching = continueWatching;
    }

    public Anime(String name, boolean watched) {
        this(0, name, "", watched, 0.0, "", LocalDateTime.now(), LocalDateTime.now(), ContentType.Anime, "", AudioType.dub, -1, -1);
    }

    public Anime(String name) {
        this(name, false);
    }

    /*
    * Creates blank anime with default data
    * */
    public Anime() {
        this(0, "", "", false, 1.0, "", LocalDateTime.now(), LocalDateTime.now(), ContentType.Anime, "", AudioType.dub, -1, -1);
    }
    //endregion

    //region bunch of generated properties
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArtworkPath() {
        return artworkPath;
    }

    public void setArtworkPath(String artworkPath) { this.artworkPath = artworkPath; }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    public LocalDateTime getLastEdit() {
        return lastEdit;
    }

    public void setLastEdit(LocalDateTime lastEdit) {
        this.lastEdit = lastEdit;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public AudioType getAudioType() {
        return audioType;
    }

    public void setAudioType(AudioType audioType) {
        this.audioType = audioType;
    }

    public long getId() { return id; }

    public void setId(long id) {
        this.id = id;
    }

    public int getEpisodeCount() { return episodeCount; }

    public void setEpisodeCount(int episodeCount) {
        this.episodeCount = episodeCount;
    }

    public int getContinueWatching() { return continueWatching; }

    public void setContinueWatching(int continueWatching) { this.continueWatching = continueWatching; }

    //endregion

    public void copyProperties(Anime other) {
        this.name = other.getName();
        this.secondName = other.getSecondName();
        this.notes = other.getNotes();
        this.rating = other.getRating();
        this.lastEdit = other.getLastEdit();
        this.seen = other.getSeen();
        this.contentType = other.getContentType();
        this.description = other.getDescription();
        this.audioType = other.getAudioType();
        this.id = other.getId();
        this.artworkPath = other.getArtworkPath();
        this.episodeCount = other.getEpisodeCount();
        this.continueWatching = other.getContinueWatching();
    }
}