package animelist.types;

public enum AudioType {
    dub,
    sub,
    none
}
