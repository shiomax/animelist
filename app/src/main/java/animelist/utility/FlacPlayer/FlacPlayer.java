package animelist.utility.FlacPlayer;

import javafx.beans.property.SimpleDoubleProperty;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public final class FlacPlayer {

    private static boolean pause = false;
    private static boolean running = true;

    private static Object lock = new Object();
    private static File openRequest = null;
    private static double seekRequest = -1;  //Either -1 or a number in [0.0, 1.0]

    //Controls
    private static float masterGainValue = 0; //value between -80db and +6db

    // Decoder state
    private static FlacDecoder dec = null;
    private static SourceDataLine line = null;
    private static long clipStartTime;

    private static SimpleDoubleProperty currentTime = new SimpleDoubleProperty();

    //Worker thread
    private static worker playerThread = null;

    //region Communication Methods

    /*
    * Kills the player, should it be running
    * */
    public static final void kill() {
        if(playerThread != null) {
            if (playerThread.isAlive()) {
                synchronized (lock) {
                    pause = false;
                    running = false;
                    lock.notifyAll();
                }
                try {
                    playerThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                playerThread = null;
            }
        }
    }

    /*
    * Opens a FLAC song
    * */
    public static final void openRequest(File file) {
        if(playerThread == null)
            playerThread = new worker();

        synchronized (lock) {
            pause = false;
            running = true;
            openRequest = file;
            lock.notifyAll();

            //If the thread is dead start it
            if(playerThread.isAlive() == false)
                playerThread.start();
        }
    }

    /*
    * Seeks possition in current song
    * */
    public static final void seekRequest(double value) {
        if(playerThread != null) {
            if (playerThread.isAlive()) {
                synchronized (lock) {
                    if (dec != null) {
                        pause = false;
                        running = true;
                        seekRequest = value;
                        lock.notifyAll();
                    }
                }
            }
        }
    }

    public static final void setMasterGain(float value) {
        synchronized (lock) {
            masterGainValue = value >= -80 && value <= 6 ? value : 0;
        }
    }

    public static final boolean getPause() {
        synchronized (lock) {
            return pause;
        }
    }

    public static final void setPause(boolean value) {
        synchronized (lock) {
            pause = value;
        }
    }

    public static final boolean togglePause() {
        synchronized (lock) {
            pause = !pause;
            lock.notifyAll();
            return pause;
        }
    }

    public static final SimpleDoubleProperty getCurrentTime() {
        return currentTime;
    }

    //endregion


    private static final class worker extends Thread {

        @Override
        public void run() {
            while (running) {
                try {
                    doWorkerIteration();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LineUnavailableException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //On pause wait for play
                synchronized (lock) {
                    while (pause && running) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    // Tries to decode+play a block of audio, process an asynchronous request, wait for a request, or throws an exception.
    private static void doWorkerIteration() throws IOException, LineUnavailableException, InterruptedException {
        // Take request from shared variables
        File openReq;
        double seekReq;
        float masterGain;
        synchronized (lock) {
            openReq = openRequest;
            openRequest = null;
            seekReq = seekRequest;
            seekRequest = -1;
            masterGain = masterGainValue;
        }

        // Open or switch files, and start audio line
        if (openReq != null) {
            seekReq = -1;
            closeFile();
            dec = new FlacDecoder(openReq);
            if (dec.numSamples == 0)
                throw new FormatException("Unknown audio length");
            AudioFormat format = new AudioFormat(dec.sampleRate, dec.sampleDepth, dec.numChannels, true, false);
            line = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, format));
            line.open(format);

            //setting up line settings
            var masterGainControl = (FloatControl)line.getControl(FloatControl.Type.MASTER_GAIN);
            masterGainControl.setValue(masterGain);


            line.start();

            //TODO: String name = openReq.getName(); can do to set name in ui or something
            clipStartTime = 0;
        } else if (dec == null) {
            synchronized (lock) {
                while (openRequest == null && running)
                    lock.wait();
            }
            return;
        }

        // Decode next audio block, or seek and decode
        long[][] samples = null;
        if (seekReq == -1) {
            Object[] temp = dec.readNextBlock();
            if (temp != null)
                samples = (long[][]) temp[0];
        } else {
            long samplePos = Math.round(seekReq * dec.numSamples);
            samples = dec.seekAndReadBlock(samplePos);
            line.flush();
            clipStartTime = line.getMicrosecondPosition() - Math.round(samplePos * 1e6 / dec.sampleRate);
        }

        // Set display position
        double timePos = (line.getMicrosecondPosition() - clipStartTime) / 1e6;
        currentTime.set(timePos * dec.sampleRate / dec.numSamples);

        // Wait when end of stream reached
        if (samples == null) {
            synchronized (lock) {
                while (openRequest == null && seekRequest == -1 && running)
                    lock.wait();
            }
            return;
        }

        // Convert samples to channel-interleaved bytes in little endian
        int bytesPerSample = dec.sampleDepth / 8;
        byte[] sampleBytes = new byte[samples[0].length * samples.length * bytesPerSample];
        for (int i = 0, k = 0; i < samples[0].length; i++) {
            for (int ch = 0; ch < samples.length; ch++) {
                long val = samples[ch][i];
                for (int j = 0; j < bytesPerSample; j++, k++)
                    sampleBytes[k] = (byte) (val >>> (j << 3));
            }
        }
        line.write(sampleBytes, 0, sampleBytes.length);
    }


    private static void closeFile() throws IOException {
        if (dec != null) {
            dec.close();
            dec = null;
        }
        if (line != null) {
            line.close();
            line = null;
        }
    }
}
