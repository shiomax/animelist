package animelist.utility.FlacPlayer;

import java.io.IOException;

// Thrown when non-conforming FLAC data is read.
public class FormatException extends IOException {
    public FormatException(String msg) {
        super(msg);
    }
}