package animelist.utility.FlacPlayer;

import java.io.*;

// Provides low-level bit/byte reading of a file.
public final class Stream {

    private RandomAccessFile raf;
    private long bytePosition;
    private InputStream byteBuffer;
    private long bitBuffer;
    private int bitBufferLen;

    public Stream(File file) throws IOException {
        raf = new RandomAccessFile(file, "r");
        seekTo(0);
    }

    public void close() throws IOException {
        raf.close();
    }

    public long getLength() throws IOException {
        return raf.length();
    }

    public long getPosition() {
        return bytePosition;
    }

    public void seekTo(long pos) throws IOException {
        raf.seek(pos);
        bytePosition = pos;
        byteBuffer = new BufferedInputStream(new InputStream() {
            public int read() throws IOException {
                return raf.read();
            }
            public int read(byte[] b, int off, int len) throws IOException {
                return raf.read(b, off, len);
            }
        });
        bitBufferLen = 0;
    }

    public int readByte() throws IOException {
        if (bitBufferLen >= 8)
            return readUint(8);
        else {
            int result = byteBuffer.read();
            if (result != -1)
                bytePosition++;
            return result;
        }
    }

    public int readUint(int n) throws IOException {
        while (bitBufferLen < n) {
            int temp = byteBuffer.read();
            if (temp == -1)
                throw new EOFException();
            bytePosition++;
            bitBuffer = (bitBuffer << 8) | temp;
            bitBufferLen += 8;
        }
        bitBufferLen -= n;
        int result = (int)(bitBuffer >>> bitBufferLen);
        if (n < 32)
            result &= (1 << n) - 1;
        return result;
    }

    public int readSignedInt(int n) throws IOException {
        return (readUint(n) << (32 - n)) >> (32 - n);
    }

    public void alignToByte() {
        bitBufferLen -= bitBufferLen % 8;
    }

}