package animelist.utility;

import org.pmw.tinylog.Logger;

/*
 * Used for occasional Performance testing.
 * */
public class Stopwatch {

    private long startTime = 0;
    private long stopTime = 0;
    private boolean running = false;

    private String jobName;

    public Stopwatch(String jobName) {
        this.jobName = jobName;
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
        this.running = true;
    }


    public void stop() {
        this.stopTime = System.currentTimeMillis();
        this.running = false;
        Logger.error(String.format("Job %s took %d ms.", jobName, getElapsedTime()));
    }


    //elaspsed time in milliseconds
    public long getElapsedTime() {
        long elapsed;
        if (running) {
            elapsed = (System.currentTimeMillis() - startTime);
        } else {
            elapsed = (stopTime - startTime);
        }
        return elapsed;
    }


    //elaspsed time in seconds
    public long getElapsedTimeSecs() {
        return getElapsedTime() / 1000;
    }

}
