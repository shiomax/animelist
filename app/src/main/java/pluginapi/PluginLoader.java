package pluginapi;

import animelist.GlobalStore;
import animelist.utility.Stopwatch;
import apicontracts.ApiClient;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;

public class PluginLoader {

    private final String PLUGIN_PATH = GlobalStore.getJarPath() + "/plugins";
    //private final String PLUGIN_PATH = "plugins";

    private PluginLoader() {
        loadPlugins();
    }

    private static PluginLoader instance = null;

    public static PluginLoader instanceOf() {
        return instance == null ? (instance = new PluginLoader()) : instance;
    }

    private List<ApiClient> apiClientList;

    public List<ApiClient> getApiClientList() {
        return apiClientList;
    }

    private final void loadPlugins() {

        this.apiClientList = new LinkedList<>();
        File pluginFolder = new File(PLUGIN_PATH);

        if (!pluginFolder.exists())
            return;

        File[] files = pluginFolder.listFiles((dir, name) -> name.endsWith(".jar"));
        ArrayList<URL> urls = new ArrayList<>();
        ArrayList<String> classes = new ArrayList<>();

        if (files != null) {
            Arrays.stream(files).forEach(file -> {
                try {
                    JarFile jarFile = new JarFile(file);
                    urls.add(new URL(String.format("jar:file:%s/%s!/", PLUGIN_PATH, file.getName())));

                    jarFile.stream().forEach(jarEntry -> {
                        if (jarEntry.getName().endsWith(".class") && jarEntry.getName().startsWith("api/")) {
                            classes.add(jarEntry.getName());
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            URLClassLoader pluginLoader = new URLClassLoader(urls.toArray(new URL[urls.size()]));
            classes.forEach(curClassString -> {
                try {
                    Class curClass = pluginLoader.loadClass(curClassString.replaceAll("/", ".").replace(".class", ""));

                    if(curClass.getSuperclass() == ApiClient.class) {
                        try {
                            this.apiClientList.add((ApiClient) curClass.getConstructor().newInstance());
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
