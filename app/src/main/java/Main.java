import fxui.AppPreloader;
import fxui.AppStart;

import static com.sun.javafx.application.LauncherImpl.*;

public class Main {

    public static void main(String[] args) {
        launchApplication(AppStart.class, AppPreloader.class, args);
    }
}
