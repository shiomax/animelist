package fxui;

import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/*
* Just a bunch of functions that are often used in several different Windows.
* */
public final class GlobalUiCollection {

    /*
    * Shows a notification on top of some window for 5 seconds
    * */
    public static final void showNotification(Window window, String text) {
        Notifications.create()
                .title("Notification")
                .text(text)
                .position(Pos.TOP_CENTER)
                .hideAfter(Duration.seconds(5))
                .owner(window)
                .darkStyle()
                .show();
    }

    public static final void showNotification(Event event, String text) {
        showNotification(((Node)event.getSource()).getScene().getWindow(), text);
    }

    /*
    * Shows an error message
    * */
    public static final void showError(String title, String message) {
        try {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle("Error");
            alert.setHeaderText(title);
            alert.setContentText(message);

            alert.showAndWait();
        } catch (IllegalStateException e) {

        }
    }
}
