package fxui;

import animelist.GlobalStore;
import animelist.io.ControllerFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.writers.FileWriter;
import pluginapi.PluginLoader;

import java.io.IOException;
import java.util.Arrays;

public class AppStart extends Application {

    private static Stage stage;
    private static Scene primaryScene;

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        stage.getIcons().add(new Image("images/icon.png"));
        if(primaryScene == null) {
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            primaryScene = new Scene(root);
        }
        stage.setTitle(GlobalStore.TITLE_MAIN);
        stage.setScene(primaryScene);
        stage.show();
    }

    public static Stage getPrimaryStage() {
        return  stage;
    }

    public static void launchApp(String[] args) {
        launch(args);
    }
}