package fxui.controllers;

import animelist.GlobalStore;
import animelist.io.ControllerFactory;
import animelist.io.IDataAccess;
import animelist.types.Anime;
import animelist.types.AnimeSource;
import fxui.GlobalUiCollection;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.controlsfx.control.CheckTreeView;
import org.controlsfx.control.PopOver;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class AnimeDetailsController {

    @FXML
    private ImageView imgViewArtwork, imgSeen;

    @FXML
    private TextArea taNotes, taDescription;

    @FXML
    private TextField tfName, tfSecondName, tfDateAdded, tfDateLastChanged, tfRating, tfContentType, tfAudioType, tfEpCount, tfEpCur;

    @FXML
    private TableView<AnimeSource> tableViewSources;

    @FXML
    private TableColumn<AnimeSource, String> tableColumnName, tableColumnScope;

    @FXML
    private TableColumn<AnimeSource, AnimeSource> tableColumnOptions;

    private ObservableList<AnimeSource> sources;
    private FilteredList<AnimeSource> filteredSources;

    private IDataAccess dataController = ControllerFactory.getDefaultController();

    private Anime anime;

    public void setAnime(Anime anime) {
        //set artwork image
        imgViewArtwork.setImage(dataController.getArtwork(anime.getId()).getImage());

        //set notes
        taNotes.setText(anime.getNotes());

        //set description
        taDescription.setText(anime.getDescription());

        //set text fields
        tfName.setText(anime.getName());
        tfSecondName.setText(anime.getSecondName());
        tfDateAdded.setText(anime.getDateAdded().format(GlobalStore.DATE_TIME_FORMATTER));
        tfDateLastChanged.setText(anime.getLastEdit().format(GlobalStore.DATE_TIME_FORMATTER));
        tfContentType.setText(anime.getContentType().toString());
        tfRating.setText(String.valueOf(anime.getRating()));
        tfAudioType.setText(anime.getAudioType().toString());
        tfEpCount.setText(anime.getEpisodeCount() >= 0 ? String.valueOf(anime.getEpisodeCount()) : "undefined");
        tfEpCur.setText(anime.getContinueWatching() >= 0 ? String.valueOf(anime.getContinueWatching()) : "undefined");
        tfEpCur.setDisable(anime.getEpisodeCount() < 0);
        tfEpCount.setDisable(anime.getEpisodeCount() < 0);

        if(anime.getSeen()) {
            imgSeen.setImage(GlobalStore.IMAGE_ANIME_SEEN);
            imgSeen.setFitHeight(GlobalStore.IMAGE_ANIME_SEEN_HEIGHT);
            imgSeen.setFitWidth(GlobalStore.IMAGE_ANIME_SEEN_WIDTH);
        } else {
            imgSeen.setImage(GlobalStore.IMAGE_ANIME_NOT_SEEN);
            imgSeen.setFitHeight(GlobalStore.IMAGE_ANIME_NOT_SEEN_HEIGHT);
            imgSeen.setFitWidth(GlobalStore.IMAGE_ANIME_NOT_SEEN_WIDTH);
        }

        this.anime = anime;

        var dbSources = ControllerFactory.getSourcesController();
        this.sources = FXCollections.observableArrayList();
        var specific = dbSources.getSources(anime.getId());
        var global = dbSources.getGlobalSources(anime.getContentType());

        if(specific != null)
            this.sources.addAll(specific);
        if(global != null)
            this.sources.addAll(global);

        this.filteredSources = new FilteredList<>(this.sources);

        this.tableViewSources.setItems(this.filteredSources);
    }

    @FXML
    public void initialize() {
        initializePopOverFilter();
        this.tableViewSources.setSelectionModel(null);
        this.tableColumnName.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getDisplayName()));
        this.tableColumnOptions.setCellValueFactory(x -> new SimpleObjectProperty<>(x.getValue()));
        this.tableColumnScope.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getAnimeId() == -1 ? "Global" : "Specific"));
        this.tableColumnOptions.setCellFactory(param -> {
            return new TableCell<AnimeSource, AnimeSource>() {
                @Override
                protected void updateItem(AnimeSource item, boolean empty) {
                    if(empty)
                        setGraphic(null);
                    else {
                        HBox box = new HBox();
                        box.setSpacing(10);
                        Button btBrowse = new Button();
                        Button btClip = new Button();
                        ImageView imageView = new ImageView(new Image("/images/inet.png"));
                        ImageView imageViewClip = new ImageView(new Image("/images/copy.png"));
                        imageView.setFitWidth(18);
                        imageView.setFitHeight(18);
                        imageViewClip.setFitHeight(19);
                        imageViewClip.setFitWidth(19);
                        btBrowse.getStylesheets().add("/css/ImageButton.css");
                        btClip.getStylesheets().add("/css/ImageButton.css");
                        btBrowse.setGraphic(imageView);
                        btClip.setGraphic(imageViewClip);
                        btBrowse.setOnAction(event -> item.openSource(anime));
                        btClip.setOnAction(event -> {
                            new Thread(() -> {
                                Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                                clpbrd.setContents(new StringSelection(item.getValue(anime)), null);
                                Platform.runLater(() -> {
                                    GlobalUiCollection.showNotification(((Node)event.getSource()).getScene().getWindow(), "Copied to Clipboard.");
                                });
                            }).start();
                        });
                        box.getChildren().addAll(btBrowse, btClip);
                        setGraphic(box);
                    }
                }
            };
        });
    }


    private CheckTreeView<String> checkTreeViewFilter;
    private CheckBoxTreeItem<String> checkBoxTreeItemRootFilter;
    private CheckBoxTreeItem<String> checkBoxTreeItemGlobal, checkBoxTreeItemSpecific;

    private void initializePopOverFilter() {
        checkBoxTreeItemRootFilter = new CheckBoxTreeItem<String>("All");
        checkBoxTreeItemGlobal = new CheckBoxTreeItem<String>("Global");
        checkBoxTreeItemSpecific = new CheckBoxTreeItem<String>("Specific");
        checkBoxTreeItemRootFilter.getChildren().addAll(checkBoxTreeItemGlobal, checkBoxTreeItemSpecific);
        checkBoxTreeItemRootFilter.setExpanded(true);
        checkBoxTreeItemRootFilter.setSelected(true);
        checkTreeViewFilter = new CheckTreeView<>(checkBoxTreeItemRootFilter);
        checkTreeViewFilter.getCheckModel().getCheckedItems().addListener(
                (ListChangeListener.Change<? extends TreeItem<String>> change) -> {
                    updateFilter();
                });
    }

    private void updateFilter() {
        this.filteredSources.setPredicate(animeSource -> {
            if(animeSource.getAnimeId() == -1)
                return this.checkBoxTreeItemGlobal.isSelected();
            return this.checkBoxTreeItemSpecific.isSelected();
        });
    }

    @FXML
    public void buttonFilterPressed(ActionEvent actionEvent) {
        PopOver popOver = new PopOver();
        popOver.setTitle("filter");
        popOver.setContentNode(checkTreeViewFilter);
        popOver.show((Node) actionEvent.getSource());
    }
}
