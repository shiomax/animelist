package fxui.controllers.settings;

import animelist.io.ControllerFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class SettingsController {

    @FXML
    private CheckBox checkKeepGlobal;

    @FXML
    private Tab tabGlobalSources;

    @FXML
    public void initialize() throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/settings/SourceSettings.fxml"));
        Parent root = fxmlLoader.load();

        SourceSettingsController controller = fxmlLoader.getController();
        controller.setup();

        tabGlobalSources.setContent(root);
    }

    @FXML
    public void keyPressedGlobal(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ESCAPE) {
            ((Node)keyEvent.getSource()).getScene().getWindow().hide();
        }
    }

    @FXML
    public void onClickResetLibrary(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog.");
        alert.setHeaderText("Deleting your library.");
        alert.setContentText("Are you ok with this?");

        var result = alert.showAndWait();

        if(result.get() == ButtonType.OK) {
            var dbController = ControllerFactory.getDefaultController();
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            if(dbController.resetLirary(checkKeepGlobal.isSelected())) {
                alert.setHeaderText("Your library was reset sucessfully.");
                alert.setContentText("Hopefully, you wanted to do that, or have a backup.");
            } else {
                alert.setHeaderText("Something went wrong.");
                alert.setContentText("Could not properly reset your library.");
            }
            alert.showAndWait();
        }
    }
}
