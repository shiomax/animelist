package fxui.controllers.settings;

import apicontracts.KeyValueStore;
import apicontracts.SettingDisplayType;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import pluginapi.PluginLoader;

import java.util.HashMap;
import java.util.Map;

public class ApiPluginSettings extends ScrollPane {

    public ApiPluginSettings() {

        this.setPadding(new Insets(10, 20, 10, 20));
        this.setFitToWidth(true);

        VBox mainBox = new VBox();
        var keyValueStore = KeyValueStore.instanceOf();

        PluginLoader.instanceOf().getApiClientList().forEach(apiClient -> {

            VBox vBox = new VBox();
            vBox.setSpacing(10);

            var header = new Label(apiClient.getDisplayName());
            header.getStylesheets().add("/css/HeadingLabel.css");
            vBox.getChildren().add(header);

            Map<TextField, String> keyMap = new HashMap<>();

            if(apiClient.getApiSettings() != null && apiClient.getApiSettings().isEmpty() == false) {
                apiClient.getApiSettings().forEach(apiSetting -> {
                    vBox.getChildren().add(new Label(apiSetting.getDisplayValue()));

                    TextField inputField;

                    if (apiSetting.getDisplayType() == SettingDisplayType.text)
                        inputField = new TextField();
                    else //SettingDisplayType.password
                        inputField = new PasswordField();

                    inputField.setText(keyValueStore.get(apiSetting.getKey()));
                    keyMap.put(inputField, apiSetting.getKey());

                    vBox.getChildren().add(inputField);
                });

                Button applyBtn = new Button("Update Settings");
                applyBtn.getStylesheets().add(String.valueOf(getClass().getResource("/css/ImageButton.css")));

                applyBtn.setOnAction(event -> {
                    for (var cur : keyMap.entrySet()) {
                        keyValueStore.persist(cur.getValue(), cur.getKey().getText());
                        cur.getKey().setText(keyValueStore.get(cur.getValue()));
                    }
                });

                vBox.getChildren().add(applyBtn);
            } else {
                vBox.getChildren().add(new Label("This plugin does not need any settings!"));
            }
            vBox.getChildren().add(new Separator());
            mainBox.getChildren().add(vBox);
        });

        this.setContent(mainBox);
    }
}
