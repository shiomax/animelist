package fxui.controllers.settings;

import animelist.io.ControllerFactory;
import animelist.types.Anime;
import animelist.types.AnimeSource;
import animelist.types.ContentType;
import animelist.types.SourceType;
import fxui.GlobalUiCollection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.controlsfx.control.CheckTreeView;

import java.util.LinkedList;
import java.util.List;

public class SourceSettingsController {

    @FXML
    private Label lbHeader, lbValidFor;

    @FXML
    private CheckBoxTreeItem<String> checkTreeBoxAll, checkTreeBoxAnime, checkTreeBoxAnimeMovie, checkTreeBoxManga, checkTreeBoxMovie, checkTreeBoxSeries, checkTreeBoxAnimeManga;

    @FXML
    private CheckTreeView<String> checkTreeViewValidFor;

    @FXML
    private GridPane gridPaneDetails;

    @FXML
    private ListView<AnimeSource> listView;
    private Integer listViewId = null;

    @FXML
    private TextField tfUrl, tfName;

    private ObservableList<AnimeSource> sourcesList;
    private List<AnimeSource> deletedCache = new LinkedList<>();

    private SourceScope sourceScope;

    //Anime for content specific adding
    private Anime anime = null;

    private String name = "New Entry";

    private AnimeSource inEdit = null;

    public void initialize() {

    }

    private void setSelection() {
        int i = this.listView.getSelectionModel().getSelectedIndex();
        this.inEdit = sourcesList.get(i);
        updateDetailsView();
    }

    private void updateDetailsView() {
        this.tfName.setText(inEdit.getSourceName() == null ? "" : inEdit.getSourceName());
        this.tfUrl.setText(inEdit.getSourceValue());
        this.gridPaneDetails.setVisible(true);

        //selection only exists for global sources
        if(this.sourceScope == SourceScope.Global) {
            unselectAll();
            for (var cur : this.inEdit.getValidForList()) {
                if (cur == ContentType.Anime)
                    this.checkTreeBoxAnime.setSelected(true);
                if (cur == ContentType.Manga)
                    this.checkTreeBoxManga.setSelected(true);
                if (cur == ContentType.AnimeManga)
                    this.checkTreeBoxAnimeManga.setSelected(true);
                if (cur == ContentType.AnimeMovie)
                    this.checkTreeBoxAnimeMovie.setSelected(true);
                if (cur == ContentType.Series)
                    this.checkTreeBoxSeries.setSelected(true);
                if (cur == ContentType.Movie)
                    this.checkTreeBoxMovie.setSelected(true);
            }
        }
    }

    private void unselectAll() {
        this.checkTreeBoxAll.setSelected(false);
        this.checkTreeBoxAnime.setSelected(false);
        this.checkTreeBoxAnimeManga.setSelected(false);
        this.checkTreeBoxSeries.setSelected(false);
        this.checkTreeBoxMovie.setSelected(false);
        this.checkTreeBoxManga.setSelected(false);
    }

    public void setup() {
        this.setup(SourceScope.Global, null);
    }

    public void setup(Anime anime) {
        this.setup(SourceScope.Specific, anime);
    }

    private void setup(SourceScope scope, Anime anime) {
        var dbController = ControllerFactory.getSourcesController();

        this.anime = anime;

        this.sourceScope = scope;

        if(this.sourceScope == SourceScope.Specific) {
            this.lbHeader.setText("Source");
            this.lbValidFor.setVisible(false);
            this.checkTreeViewValidFor.setVisible(false);
            if(this.anime != null)
                this.sourcesList = FXCollections.observableArrayList(dbController.getSources(this.anime.getId()));
            else
                this.sourcesList = FXCollections.observableArrayList();
        } else {
            this.sourcesList = FXCollections.observableArrayList(dbController.getAllGlobalSources());
        }

        this.listView.setItems(this.sourcesList);

        this.listView.setCellFactory(param -> new ListCell<>(){
            @Override
            protected void updateItem(AnimeSource item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getSourceName() == null) {
                    setGraphic(null);
                } else {
                    setGraphic(new Label(item.getDisplayName()));
                }
            }
        });

        this.listView.setOnMousePressed(event -> {
            if(this.listView.getSelectionModel().isEmpty() == false && event.isSecondaryButtonDown()) {
                ContextMenu contextMenu = new ContextMenu();
                MenuItem menuItem = new MenuItem("delete");
                contextMenu.getItems().add(menuItem);
                var item = this.listView.getSelectionModel().getSelectedItem();
                menuItem.setOnAction(e -> {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure?");
                    var result = alert.showAndWait();
                    if(result.get() == ButtonType.OK) {
                        if(anime == null) {
                            if (item.getId() != null && item.getId() != 0)
                                dbController.deleteSource(item.getId());
                        } else {
                            this.deletedCache.add(item);
                        }
                        this.sourcesList.remove(item);
                        this.gridPaneDetails.setVisible(false);
                    }
                });
                this.listView.setContextMenu(contextMenu);
            }
        });
    }

    //region Button-Events

    public void btCancelClick(ActionEvent actionEvent) {
        this.gridPaneDetails.setVisible(false);
    }

    public void btApplyClick(ActionEvent actionEvent) {
        String name = this.tfName.getText();
        String url = this.tfUrl.getText();

        if(url == null || url.isEmpty()) {
            GlobalUiCollection.showError("Error", "URL cannot be empty!");
        } else {
            //if name is empty url will be used.
            if(this.name.isEmpty())
                this.name = url;

            List<ContentType> types = null;

            //selection only exists for global sources
            if(this.sourceScope == SourceScope.Global) {
                types = new LinkedList<>();

                if (this.checkTreeBoxAnime.isSelected())
                    types.add(ContentType.Anime);
                if (this.checkTreeBoxAnimeMovie.isSelected())
                    types.add(ContentType.AnimeMovie);
                if (this.checkTreeBoxManga.isSelected())
                    types.add(ContentType.Manga);
                if (this.checkTreeBoxMovie.isSelected())
                    types.add(ContentType.Movie);
                if (this.checkTreeBoxSeries.isSelected())
                    types.add(ContentType.Series);
                if (this.checkTreeBoxAnimeManga.isSelected())
                    types.add(ContentType.AnimeManga);
            }

            var controller = ControllerFactory.getSourcesController();

            inEdit.setSourceName(name);
            inEdit.setTarget(url, types, SourceType.HttpLink);


            switch (this.sourceScope) {
                case Global:

                    if(inEdit.getId() == null) {
                        inEdit = (AnimeSource) controller.putSource(inEdit);
                        this.sourcesList.add(inEdit);
                    } else {
                        controller.updateSource(inEdit);
                        this.listView.refresh();
                    }

                    break;
                case Specific:

                    if(inEdit.getId() == null) {
                        inEdit.setId(-1);
                        this.sourcesList.add(inEdit);
                    } else {
                        this.listView.refresh();
                    }

                    break;
            }
        }
    }

    /*
    * Specific Sources will be save with this methode, while global sources are saved as soon as
    * they are created.
    * */
    public void saveSources(Anime anime) {
        var controller = ControllerFactory.getSourcesController();

        this.deletedCache.forEach(animeSource -> {
            if (animeSource.getId() != null && animeSource.getId() > 0)
                controller.deleteSource(animeSource.getId());
        });

        for(var cur : this.sourcesList) {
            if(cur.getId() <= 0)
                cur.setId(null);

            if(cur.getId() == null) {
                cur.setAnimeId(anime.getId());
                controller.putSource(cur);
            }
            else
                controller.updateSource(cur);
        }
    }

    public void btNewClick(ActionEvent actionEvent) {
        this.inEdit = new AnimeSource();
        if(this.sourceScope == SourceScope.Global)
            this.inEdit.setValidAll();
        this.gridPaneDetails.setVisible(true);
        this.updateDetailsView();
    }

    public void onClickListView(MouseEvent event) {
        if(listView.getSelectionModel().isEmpty() == false &&
                (listViewId == null || listView.getSelectionModel().getSelectedIndex() != listViewId)) {
            this.listViewId = listView.getSelectionModel().getSelectedIndex();
            setSelection();
        }
    }

    //endregion

    public enum SourceScope {
        Global,
        Specific
    }
}
