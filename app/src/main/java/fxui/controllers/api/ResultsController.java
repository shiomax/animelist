package fxui.controllers.api;

import animelist.io.ControllerFactory;
import animelist.types.Anime;
import apicontracts.ApiClient;
import apicontracts.IApiEntity;
import fxui.controllers.MainController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ResultsController {

    @FXML
    private ListView<IApiEntity> lvResults;

    private List<IApiEntity> searchResults;
    private ObservableList<IApiEntity> observableList;

    private ApiClient apiClient;
    private Anime anime;

    @FXML
    private VBox boxOverview;

    @FXML
    private BorderPane boxDetails;

    public void initSearchResults(List<IApiEntity> searchResults) {
        this.searchResults = searchResults;

        this.observableList = FXCollections.observableArrayList(searchResults);

        lvResults.setItems(observableList);

        lvResults.setCellFactory(param -> new customListCell());

        lvResults.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.lvResults.refresh();
        });
    }

    public void initAnime(Anime anime) {
        var dataController = ControllerFactory.getDefaultController();
        this.anime = anime;
        this.imgViewOld.setImage(dataController.getArtwork(anime.getId()).getImage());
    }

    public void initApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    private class customListCell extends ListCell<IApiEntity> {
        @Override
        protected void updateItem(IApiEntity item, boolean empty) {
            super.updateItem(item, empty);

            if (!empty) {
                if (lvResults.getSelectionModel().getSelectedItem() == item) {
                    HBox hBox = new HBox();
                    hBox.setSpacing(10);
                    ImageView imageView = new ImageView(
                            new Image("/images/noimage.jpg")
                    );
                    if(item.getImageOptions() != null) {
                        var optImg = item.getImageOptions().stream().findFirst();
                        if (optImg.isPresent()) {
                            imagePreviewLoadThread iplt = new imagePreviewLoadThread(optImg.get(), imageView, lvResults.getSelectionModel().getSelectedIndex());
                            iplt.start();
                        }
                    }
                    imageView.setFitHeight(332 / 2);
                    imageView.setFitWidth(225 / 2);
                    VBox vBox = new VBox();
                    vBox.setSpacing(5);
                    item.getNameOptions().forEach(curName -> {
                        vBox.getChildren().add(new Label(curName));
                    });

                    hBox.getChildren().addAll(imageView, vBox);
                    this.setText(null);

                    hBox.setOnMouseClicked(event -> {
                        if (event.getClickCount() == 2 && event.getButton() == MouseButton.PRIMARY) {
                            updateDetailsView();
                        }
                    });

                    this.setGraphic(hBox);
                } else {
                    var optName = item.getNameOptions().stream().findFirst();

                    if (optName.isPresent()) {
                        this.setGraphic(null);
                        this.setText(optName.get());
                    } else {
                        this.setText(null);
                        this.setGraphic(null);
                    }
                }
            } else {
                this.setText(null);
                this.setGraphic(null);
            }
        }
    }

    private List<RadioButton> secondNameOptionsBtns;

    @FXML
    private ImageView imgViewOld, imgViewNew, imgViewNewImageCheck, imgViewOldImageCheck, imgViewNewDesCheck, imgViewOldDesCheck;

    @FXML
    private TextArea taNewDescription, taOldDescription;
    
    private boolean newImage = false;
    private boolean newDescription = false;

    public void onClickNewImage(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1
                && !newImage) {
            newImage = true;
            imgViewOldImageCheck.setVisible(false);
            imgViewNewImageCheck.setVisible(true);
        }
    }

    public void onClickOldImage(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1
                && newImage) {
            newImage = false;
            imgViewOldImageCheck.setVisible(true);
            imgViewNewImageCheck.setVisible(false);
        }
    }

    public void onClickOldDescription(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1
                && newDescription) {
            newDescription = false;
            imgViewOldDesCheck.setVisible(true);
            imgViewNewDesCheck.setVisible(false);
        }
    }

    public void onClickNewDescription(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1
                && newDescription == false) {
            newDescription = true;
            imgViewOldDesCheck.setVisible(false);
            imgViewNewDesCheck.setVisible(true);
        }
    }

    @FXML
    private ComboBox cbNewNames, cbNewSecondName;

    @FXML
    private CheckBox checkNewSecondName, checkNewName;

    @FXML
    private Label lbCurrentName, lbCurrentSecondName;

    private void updateDetailsView() {

        resetDetailsView();

        var selectedItem = lvResults.getSelectionModel().
                getSelectedItem();

        imgViewNew.setImage(new Image(
                selectedItem.getImageOptions()
                        .stream().findFirst().get()
        ));

        //Name Options
        this.lbCurrentName.setText(anime.getName());
        cbNewNames.getItems().addAll(selectedItem.getNameOptions());

        //Second Name Options
        this.lbCurrentSecondName.setText(anime.getSecondName().isEmpty() ? "<empty>" : anime.getSecondName());
        cbNewSecondName.getItems().addAll(selectedItem.getNameOptions());

        this.taOldDescription.setText(this.anime.getDescription());
        this.taNewDescription.setText(selectedItem.getDescriptionOptions()
                .stream().findFirst().get());

        boxOverview.setVisible(false);
        boxDetails.setVisible(true);
    }

    private void resetDetailsView() {
        cbNewNames.getItems().clear();
        cbNewSecondName.getItems().clear();
        this.newDescription = false;
        this.newImage = false;
        this.imgViewNewImageCheck.setVisible(false);
        this.imgViewOldImageCheck.setVisible(true);
        this.imgViewNewDesCheck.setVisible(false);
        this.imgViewOldDesCheck.setVisible(true);
        this.checkNewName.setSelected(false);
        this.checkNewSecondName.setSelected(false);
    }

    public void initialize() {
        this.checkNewName.setOnMouseClicked(event -> {
            if(event.getButton() == MouseButton.PRIMARY && this.checkNewName.isSelected()) {
                this.checkNewName.setSelected(false);
            } else if (!this.checkNewName.isSelected()) {
                if(!this.cbNewNames.getSelectionModel().isEmpty())
                    this.cbNewNames.getSelectionModel().clearSelection();
            }
        });
        this.checkNewSecondName.setOnMouseClicked(event -> {
            if(event.getButton() == MouseButton.PRIMARY && this.checkNewSecondName.isSelected()) {
                this.checkNewSecondName.setSelected(false);
            } else if(!this.checkNewSecondName.isSelected()) {
                if(!this.cbNewSecondName.getSelectionModel().isEmpty())
                    this.cbNewSecondName.getSelectionModel().clearSelection();
            }
        });
        this.cbNewNames.setOnAction(event -> {
            if(!this.cbNewNames.getSelectionModel().isEmpty() &&
                    !((String) this.cbNewNames.getValue()).isEmpty()) {
                this.checkNewName.setSelected(true);
            }
        });
        this.cbNewSecondName.setOnAction(event -> {
            if(!this.cbNewSecondName.getSelectionModel().isEmpty() &&
                    !((String) this.cbNewSecondName.getValue()).isEmpty()) {
                this.checkNewSecondName.setSelected(true);
            }
        });
    }

    public void btImportClick(ActionEvent actionEvent) {
        var dbController = ControllerFactory.getDefaultController();

        if(!this.cbNewNames.getSelectionModel().isEmpty()) {
            String newName = (String)this.cbNewNames.getValue();
            if(!newName.isEmpty()) {
                this.anime.setName(newName);
            }
        }

        if(!this.cbNewSecondName.getSelectionModel().isEmpty()) {
            String newSecondName = (String)this.cbNewSecondName.getValue();
            if(!newSecondName.isEmpty()) {
                this.anime.setSecondName(newSecondName);
            }
        }

        if(newDescription) {
            this.anime.setDescription(this.taNewDescription.getText());
        }

        dbController.updateAnime(anime);

        if(newImage) {
            String urlStr = lvResults.getSelectionModel()
                    .getSelectedItem().getImageOptions()
                    .stream().findFirst().get();
            URL imgUrl = null;
            try {
                imgUrl = new URL(urlStr);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if(imgUrl != null) {
                dbController.updateAnimeArtworkFromURL(anime, imgUrl);
            }
        }

        MainController.updateCell();

        ((Node)actionEvent.getSource()).getScene().getWindow().hide();
    }

    private static List<Integer> invalidURLs = new LinkedList<>();

    private class imagePreviewLoadThread extends Thread {

        private String path;
        private ImageView imageView;
        private int index;

        public imagePreviewLoadThread(String path, ImageView imageView, int index) {
            this.path = path;
            this.imageView = imageView;
            this.index = index;
        }

        @Override
        public void run() {
            if(!invalidURLs.contains(index)) {
                try {
                    Image image = new Image(path);
                    Platform.runLater(() -> {
                        if (lvResults.getSelectionModel().getSelectedIndex() == index) {
                            this.imageView.setImage(image);
                        }
                    });
                } catch (IllegalArgumentException ex) {
                    invalidURLs.add(index);
                }
            }
        }
    }

    @FXML
    public void onClickPrevious(ActionEvent actionEvent) {
        if (boxDetails.isVisible()) {
            boxDetails.setVisible(false);
            boxOverview.setVisible(true);
        } else {
            ((Node) actionEvent.getSource()).getScene().getWindow().hide();

            SearchController.showSearchView(this.anime, this.apiClient);
        }
    }
}
