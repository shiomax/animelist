package fxui.controllers.api;

import animelist.types.Anime;
import apicontracts.ApiClient;
import apicontracts.IApiEntity;
import apicontracts.SearchResult;
import fxui.AppStart;
import fxui.GlobalUiCollection;
import fxui.custom.SearchingLabel;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.List;

public class SearchController {

    @FXML
    private Label lbTitle;

    @FXML
    private VBox searchView, searchingView;

    @FXML
    private SearchingLabel searchingLabel;

    @FXML
    private TextField tfSearch;

    private Anime anime;

    private ApiClient apiClient;

    public void initialize() {
        this.tfSearch.setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.ENTER) {
                btSearchClick(event);
            }
        });
    }

    public void btSearchClick(Event actionEvent) {
        this.searchView.setDisable(true);
        this.searchingView.setVisible(true);

        this.searchThread = new searchThread();
        this.searchingLabel.startAnnimation();

        this.searchThread.initSearch(tfSearch.getText());
        this.searchThread.start();
    }

    public void searchCompleted(SearchResult results) {

        if(results == null || !results.isSuccess() || results.getValue().stream().findAny().isEmpty()) {
            GlobalUiCollection.showNotification(this.lbTitle.getScene().getWindow(),
                    results.isSuccess() ? "No results." : results.getErrorMessage());
        } else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/api/Results.fxml"));
            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (root != null) {
                Stage stage = new Stage();
                stage.setTitle(String.format("AnimeList - %s - SearchResults", this.apiClient.getDisplayName()));

                var controller = (ResultsController) loader.getController();
                controller.initSearchResults(results.getValue());
                controller.initApiClient(apiClient);
                controller.initAnime(anime);

                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.UTILITY);
                stage.initOwner(AppStart.getPrimaryStage());

                stage.setScene(new Scene(root));
                stage.setOnCloseRequest(event -> {
                    SearchController.showSearchView(anime, apiClient);
                });

                this.lbTitle.getScene().getWindow().hide();
                stage.show();
            }
        }
    }

    public void setup(Anime anime, ApiClient apiClient) {
        this.lbTitle.setText(apiClient.getDisplayName() + " - Search");
        this.apiClient = apiClient;
        this.anime = anime;
        this.tfSearch.setText(this.anime.getName());
    }

    private searchThread searchThread;

    private class searchThread extends Thread {

        private volatile boolean running = true;

        private String search;
        private SearchResult results;

        public void initSearch(String search) {
            this.search = search;
            this.running = true;
        }

        public boolean isRunning() {
            return running;
        }

        @Override
        public void run() {
            results = apiClient.search(search);

            this.running = false;
            Platform.runLater(() -> {
                searchingLabel.stopAnnimation();
                searchView.setDisable(false);
                searchingView.setVisible(false);
                searchCompleted(this.results);
            });
        }
    }

    public static void showSearchView(Anime anime, ApiClient apiClient) {
        Stage stage = new Stage();
        stage.initOwner(AppStart.getPrimaryStage());
        stage.initStyle(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("AnimeList - " + apiClient.getDisplayName());

        FXMLLoader loader = new FXMLLoader(SearchController.class.getResource("/fxml/api/Search.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        var controller = (SearchController) loader.getController();
        controller.setup(anime, apiClient);

        stage.setScene(new Scene(root));
        stage.show();
    }
}
