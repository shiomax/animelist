package fxui.controllers;

import animelist.GlobalStore;
import animelist.io.ControllerFactory;
import animelist.io.IDataAccess;
import animelist.types.Anime;
import animelist.types.ContentType;
import fxui.AppStart;
import fxui.GlobalUiCollection;
import fxui.controllers.api.SearchController;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import org.controlsfx.control.CheckTreeView;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.ToggleSwitch;
import pluginapi.PluginLoader;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Predicate;

public class MainController {

    private static MainController instance = null;

    public static void updateCell() {
        instance.doUpdateCell();
    }

    private void doUpdateCell() {
        animes.refresh();
    }

    @FXML
    private TableView<Anime> animes;

    @FXML
    private TextField tf_search;

    @FXML
    private ToggleSwitch ts_watchlist;

    @FXML
    private Label lbBottomInfo;

    @FXML
    private BorderPane detailsPane, mainPane;

    private IDataAccess dataController;

    //region buttonEvents
    @FXML
    public void buttonAddClicked(Event event) {
        Stage stage = new Stage();
        stage.initOwner(AppStart.getPrimaryStage());
        stage.initStyle(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(GlobalStore.TITLE_ADD_ANIME);
        stage.setResizable(false);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AnimeAddOrEdit.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        var controller = (AnimeAddOrEditController) loader.getController();
        controller.setup(null);

        stage.setScene(new Scene(root));
        stage.showAndWait();
    }

    @FXML
    public void buttonOpenClicked(Event event) {
        buttonDetailsClicked();
    }

    public void buttonDetailsClicked() {
        Anime item = animes.getSelectionModel().getSelectedItem();

        BoxBlur bb = new BoxBlur();
        bb.setWidth(3);
        bb.setHeight(3);
        bb.setIterations(3);

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/AnimeDetails.fxml"));
            Parent root = fxmlLoader.load();
            AnimeDetailsController controller = fxmlLoader.getController();
            controller.setAnime(item);

            this.detailsPane.setCenter(root);
            this.detailsPane.setVisible(true);
            this.mainPane.setDisable(true);
            this.mainPane.setEffect(bb);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void btCloseDetailsClick(ActionEvent actionEvent) {
        this.detailsPane.setVisible(false);
        this.detailsPane.setCenter(null);
        this.mainPane.setEffect(null);
        this.mainPane.setDisable(false);
    }

    @FXML
    public void buttonDeleteClicked(Event event) {
        Anime anime = animes.getSelectionModel().getSelectedItem();
        if (anime != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure?", ButtonType.YES, ButtonType.NO);
            alert.setTitle("confirm delete");
            alert.setHeaderText(null);
            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.YES) {
                dataController.deleteAnime(anime);
            }
        }
    }

    @FXML
    public void buttonEditClicked(Event event) {
        Anime anime = animes.getSelectionModel().getSelectedItem();
        if (anime != null) {
            Stage stage = new Stage();
            stage.initOwner(AppStart.getPrimaryStage());
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle(GlobalStore.TITLE_EDIT_ANIME);
            stage.setResizable(false);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AnimeAddOrEdit.fxml"));
            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            AnimeAddOrEditController controller = loader.getController();
            controller.setup(anime);

            stage.setScene(new Scene(root));
            stage.showAndWait();

            if(detailsPane.isVisible())
                buttonDetailsClicked();
        } else {
            GlobalUiCollection.showNotification(event, "No anime selected.");
        }
    }
    //endregion

    //region MenuItems
    @FXML
    public void menuItemSettingsAction(Event event) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/settings/Settings.fxml"));
            Stage stage = new Stage();
            stage.initOwner(AppStart.getPrimaryStage());
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.setTitle(GlobalStore.TITLE_SETTINGS);
            stage.setScene(new Scene(root));
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void menuItemExportAction(ActionEvent actionEvent) {
        Parent root = null;

        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/export/Export.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (root != null) {
            Stage stage = new Stage();
            stage.initOwner(AppStart.getPrimaryStage());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UTILITY);

            stage.setTitle("AnimeList - Export");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.showAndWait();
        }
    }

    @FXML
    public void menuImportAction(ActionEvent actionEvent) {
        Parent root = null;

        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/export/Import.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (root != null) {
            Stage stage = new Stage();
            stage.initOwner(AppStart.getPrimaryStage());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UTILITY);

            stage.setTitle("AnimeList - Import");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.showAndWait();
        }
    }

    @FXML
    public void menuItemNewAnimeAction(Event event) {
        buttonAddClicked(event);
    }

    @FXML
    public void menuItemOpenAnimeAction(Event event) {
        buttonOpenClicked(event);
    }

    @FXML
    public void menuItemEditAnimeAction(Event event) {
        buttonEditClicked(event);
    }

    private static boolean aboutStageRunning = false;

    @FXML
    public void menuAbout(Event event) {
        if (!aboutStageRunning) {
            aboutStageRunning = true;
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/fxml/About.fxml"));

                Parent root = fxmlLoader.load();
                var controller = fxmlLoader.<AboutController>getController();

                Stage stage = new Stage();
                //stage.initOwner(AppStart.getPrimaryStage());
                stage.initStyle(StageStyle.UTILITY);
                stage.setTitle(GlobalStore.TITLE_ABOUT);
                stage.initModality(Modality.NONE);

                stage.setScene(new Scene(root));

                stage.setOnHidden(x -> {
                    controller.killAudioWorker();
                    aboutStageRunning = false;
                });
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //endregion

    //region filter and sort
    private CheckTreeView<String> checkTreeViewFilter;
    private CheckBoxTreeItem<String> checkBoxTreeItemRootFilter;
    private CheckBoxTreeItem<String> checkBoxTreeItemAnime, checkBoxTreeItemAnimeMovie, checkBoxTreeItemManga, checkBoxTreeItemMovie, checkBoxTreeItemSeries;

    private void initializePopOverFilter() {
        checkBoxTreeItemRootFilter = new CheckBoxTreeItem<String>("All");
        checkBoxTreeItemAnime = new CheckBoxTreeItem<String>("Anime");
        checkBoxTreeItemAnimeMovie = new CheckBoxTreeItem<String>("AnimeMovie");
        checkBoxTreeItemManga = new CheckBoxTreeItem<String>("Manga");
        checkBoxTreeItemMovie = new CheckBoxTreeItem<String>("Movie");
        checkBoxTreeItemSeries = new CheckBoxTreeItem<String>("Series");
        checkBoxTreeItemRootFilter.getChildren().addAll(checkBoxTreeItemAnime, checkBoxTreeItemAnimeMovie, checkBoxTreeItemManga, checkBoxTreeItemMovie, checkBoxTreeItemSeries);
        checkBoxTreeItemRootFilter.setExpanded(true);
        checkBoxTreeItemRootFilter.setSelected(true);
        checkTreeViewFilter = new CheckTreeView<>(checkBoxTreeItemRootFilter);
        checkTreeViewFilter.getCheckModel().getCheckedItems().addListener(
                (ListChangeListener.Change<? extends TreeItem<String>> change) -> {
                    updateFilter();
                });
    }

    @FXML
    public void buttonFilterPressed(Event event) {
        PopOver popOver = new PopOver();
        popOver.setTitle("filter");
        popOver.setContentNode(checkTreeViewFilter);
        popOver.show((Node) event.getSource());
    }

    @FXML
    public void tfSearchChanged() {
        this.updateFilter();
    }

    @FXML
    public void filterChanged() {
        this.updateFilter();
    }

    private void updateFilter() {
        boolean watchlist = ts_watchlist.isSelected();
        boolean displayAnimes = checkBoxTreeItemAnime.isSelected();
        boolean displayMangas = checkBoxTreeItemManga.isSelected();
        boolean displayAnimeMovies = checkBoxTreeItemAnimeMovie.isSelected();
        boolean displayMovies = checkBoxTreeItemMovie.isSelected();
        boolean displaySeries = checkBoxTreeItemSeries.isSelected();
        boolean displayAnimeManga = checkBoxTreeItemManga.isSelected() || checkBoxTreeItemAnime.isSelected();

        dataController.getAllFiltered().setPredicate(new Predicate<Anime>() {
            @Override
            public boolean test(Anime anime) {
                if ((anime.getContentType() == ContentType.Anime && !displayAnimes)
                        || (anime.getContentType() == ContentType.AnimeMovie && !displayAnimeMovies)
                        || (anime.getContentType() == ContentType.Manga && !displayMangas)
                        || (anime.getContentType() == ContentType.Movie && !displayMovies)
                        || (anime.getContentType() == ContentType.Series && !displaySeries)
                        || (anime.getContentType() == ContentType.AnimeManga && !displayAnimeManga)) {
                    return false;
                }
                if (watchlist && anime.getSeen()) {
                    return false;
                }
                String search = tf_search.getText();
                if (search.equals("")) {
                    return true;
                } else {
                    return anime.getName().toLowerCase().contains(search.toLowerCase()) || anime.getSecondName().toLowerCase().contains(search.toLowerCase());
                }
            }
        });
    }
    //endregion

    public void initialize() {
        dataController = ControllerFactory.getDefaultController();

        MainController.instance = this;
        initializePopOverFilter();
        initializeGridCells();

        this.lbBottomInfo.setText("0 entries");

        this.dataController.librarySize().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> {
                this.lbBottomInfo.setText(newValue + " entries");
            });
        });

        inizializeGridViewMouseHandler();
        animes.setOnMousePressed(gridViewMouseHandler);

        SortedList<Anime> list = dataController.getAllSorted();

        animes.setItems(list);

        list.comparatorProperty().bind(animes.comparatorProperty());

        setupContextMenu();
    }

    //region ContextMenu (pop up when right-clicking the TableView)
    private ContextMenu contextMenu;
    private MenuItem contextMenuEdit, contextMenuDelete, contextMenuPreview;
    private Menu contextMenuApi;

    private void setupContextMenu() {
        this.contextMenu = new ContextMenu();
        this.contextMenuEdit = new MenuItem("(crtl+e)", new Label("Edit"));
        this.contextMenuEdit.setOnAction(event -> {
            //NOTE: to fix an issue where the context menu would not auto hide on linux
            this.contextMenu.hide();
            buttonEditClicked(event);
        });
        this.contextMenuDelete = new MenuItem("(del)", new Label("Delete"));
        this.contextMenuDelete.setOnAction(event -> {
            this.contextMenu.hide();
            buttonDeleteClicked(event);
        });
        this.contextMenuPreview = new MenuItem("(crtl+p | crtl+d)", new Label("Preview"));
        this.contextMenuPreview.setOnAction(event -> {
            this.contextMenu.hide();
            buttonOpenClicked(event);
        });
        this.contextMenuApi = new Menu("", new Label("Import (APIs)"));

        PluginLoader.instanceOf().getApiClientList().forEach(apiClient -> {
            MenuItem curMi = new MenuItem(apiClient.getDisplayName());
            curMi.setOnAction(event -> {
                if(!this.animes.getSelectionModel().isEmpty() &&
                        this.animes.getSelectionModel().getSelectedItem() != null) {
                    this.contextMenu.hide();
                    SearchController.showSearchView(
                            this.animes.getSelectionModel().getSelectedItem(),
                            apiClient);
                }
            });
            this.contextMenuApi.getItems().add(curMi);
        });

        contextMenu.getItems().addAll(contextMenuPreview, new SeparatorMenuItem(), contextMenuEdit, contextMenuApi, new SeparatorMenuItem(), contextMenuDelete);
        this.animes.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.SECONDARY)
                animes.setContextMenu(contextMenu);
        });
    }
    //endregion

    private EventHandler<MouseEvent> gridViewMouseHandler;

    private void inizializeGridViewMouseHandler() {
        gridViewMouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2 && !animes.getSelectionModel().isEmpty()) {
                    buttonDetailsClicked();
                }
            }
        };
    }

    //region CELLS_GRID
    @FXML
    private TableColumn<Anime, String> gridCell_Name, gridCell_SecondName, gridCell_Rating,
            gridCell_DateAdded, gridCell_LastChanged, gridCell_ContentType, gridCell_AudioType;
    @FXML
    private TableColumn<Anime, Boolean> gridCell_Seen;

    @FXML
    private TableColumn<Anime, Integer> gridCell_epCount, gridCell_curEp;

    private void initializeGridCells() {

        gridCell_epCount.setCellFactory(new Callback<TableColumn<Anime, Integer>, TableCell<Anime, Integer>>() {
            @Override
            public TableCell<Anime, Integer> call(TableColumn<Anime, Integer> param) {
                TableCell<Anime, Integer> cell = new TableCell<>() {
                    @Override
                    protected void updateItem(Integer item, boolean empty) {
                        setGraphic(empty || item < 0 ? null : new Text(item.toString()));
                    }
                };
                return cell;
            }
        });

        gridCell_curEp.setCellFactory(new Callback<TableColumn<Anime, Integer>, TableCell<Anime, Integer>>() {
            @Override
            public TableCell<Anime, Integer> call(TableColumn<Anime, Integer> param) {
                TableCell<Anime, Integer> cell = new TableCell<>() {
                    @Override
                    protected void updateItem(Integer item, boolean empty) {
                        setGraphic(empty || item < 0 ? null : new Text(item.toString()));
                    }
                };
                return cell;
            }
        });

        gridCell_Seen.setCellFactory(new Callback<TableColumn<Anime, Boolean>, TableCell<Anime, Boolean>>() {
            @Override
            public TableCell<Anime, Boolean> call(TableColumn<Anime, Boolean> param) {
                TableCell<Anime, Boolean> cell = new TableCell<Anime, Boolean>() {
                    @Override
                    protected void updateItem(Boolean item, boolean empty) {
                        if (!empty) {
                            setGraphic(item ? GlobalStore.getAnimeSeenImageView() : GlobalStore.getAnimeNotSeenImageView());
                        } else {
                            setGraphic(null);
                        }
                    }
                };
                return cell;
            }
        });

        gridCell_Name.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        gridCell_SecondName.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getSecondName()));
        gridCell_AudioType.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(cellData.getValue().getAudioType())));
        gridCell_Seen.setCellValueFactory(cellData -> new ReadOnlyBooleanWrapper(cellData.getValue().getSeen()));
        gridCell_ContentType.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(cellData.getValue().getContentType())));
        gridCell_Rating.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(cellData.getValue().getRating())));
        gridCell_DateAdded.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getDateAdded().format(GlobalStore.DATE_TIME_FORMATTER)));
        gridCell_LastChanged.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getLastEdit().format(GlobalStore.DATE_TIME_FORMATTER)));
        gridCell_epCount.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getEpisodeCount()).asObject());
        gridCell_curEp.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getContinueWatching()).asObject());
    }

    @FXML
    public void tableViewKeyPresed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.DELETE) {
            buttonDeleteClicked(keyEvent);
        } else if (keyEvent.isControlDown()) {
            if (keyEvent.getCode() == KeyCode.D || keyEvent.getCode() == KeyCode.P) {
                buttonOpenClicked(keyEvent);
            } else if (keyEvent.getCode() == KeyCode.A || keyEvent.getCode() == KeyCode.N) {
                buttonAddClicked(keyEvent);
            } else if (keyEvent.getCode() == KeyCode.E) {
                buttonEditClicked(keyEvent);
            }
        }
    }

    /*
     * Global key pressed event on stack pane
     * */
    public void keyPressedGlobal(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ESCAPE) {
            btCloseDetailsClick(null);
        }
    }
    //endregion
}
