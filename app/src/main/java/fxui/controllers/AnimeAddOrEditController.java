package fxui.controllers;

import animelist.GlobalStore;
import animelist.io.ControllerFactory;
import animelist.io.IDataAccess;
import animelist.types.Anime;
import animelist.types.AudioType;
import animelist.types.ContentType;
import fxui.GlobalUiCollection;
import fxui.controllers.settings.SourceSettingsController;
import fxui.custom.RatingControl;
import fxui.custom.SegmentableButton;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;

public class AnimeAddOrEditController {

    @FXML
    private Button bt_deleteImage;

    @FXML
    private TextField tf_name, tf_secondName, tf_epCount, tf_continueAt;

    @FXML
    private ComboBox cb_type, cb_audio;

    @FXML
    private SegmentableButton tbSeenYes, tbSeenNo;

    @FXML
    private RatingControl rating;

    @FXML
    private TextArea ta_notes, ta_description;

    @FXML
    private ImageView imgArtwork;

    @FXML
    private Tab tabSources;

    private IDataAccess dataController = ControllerFactory.getDefaultController();
    private SourceSettingsController sourceController;
    private File artwork = null;
    private boolean artworkChanged = false;
    private Anime anime = null;

    private static final String CONTINUE_AT_MSG = "ContinueAt must be smaller than episodeCount if set.";

    @FXML
    public void buttonAddClicked(Event event) {
        String name = tf_name.getText();
        if (!name.equals("")) {
            if (this.anime != null || dataController.getAll().stream().noneMatch(x -> x.getName().equals(name))) {
                String secondName = tf_secondName.getText();
                boolean seen = this.tbSeenYes.isSelected();
                Double r = (double) rating.getRating();
                String notes = ta_notes.getText();
                String description = ta_description.getText();
                ContentType contentType = ContentType.valueOf((String) cb_type.getValue());
                AudioType audioType = AudioType.valueOf((String) cb_audio.getValue());
                int episodeCount = tf_epCount.getText().isEmpty() ? -1 : Integer.valueOf(tf_epCount.getText());
                int continueWatching = tf_continueAt.getText().isEmpty() ? -1 : Integer.valueOf(tf_continueAt.getText());

                if (continueWatching != -1 && episodeCount != -1 && continueWatching > episodeCount) {
                    GlobalUiCollection.showNotification(this.cb_audio.getScene().getWindow(), CONTINUE_AT_MSG);
                } else {

                    Anime anime = new Anime(0, name, secondName, seen, r, notes,
                            LocalDateTime.now(), LocalDateTime.now(), contentType, description, audioType, episodeCount, continueWatching);

                    if (this.anime == null) { //create
                        anime.setId(dataController.addAnime(anime, artwork));
                    } else { //edit
                        anime.setId(this.anime.getId());
                        anime.setArtworkPath(this.anime.getArtworkPath());
                        if (artworkChanged)
                            dataController.updateAnimeArtworkFromFile(anime, this.artwork);
                        dataController.updateAnime(anime);
                    }

                    this.sourceController.saveSources(anime);
                    MainController.updateCell();
                    ((Node) event.getSource()).getScene().getWindow().hide();
                }
            } else {
                showNotification(event, String.format("Anime with name\n%s\nalready exists!\nName field must be unique!", name));
            }
        } else {
            showNotification(event, "Name field cannot be empty!");
        }
    }

    @FXML
    public void buttonDeleteImageClicked(Event event) {
        if (this.artwork != null) {
            this.imgArtwork.setImage(GlobalStore.NO_IMAGE_IMAGE);
            this.artwork = null;
            this.artworkChanged = true;
        }
    }

    private void showNotification(Event event, String text) {
        Notifications.create()
                .title("Notification")
                .text(text)
                .position(Pos.TOP_CENTER)
                .hideAfter(Duration.seconds(5))
                .owner(((Node) (event.getSource())).getScene().getWindow())
                .darkStyle()
                .show();
    }

    @FXML
    public void buttonCancelClicked(Event event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    public void buttonChooseArtClicked(Event event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Artwork");
        fileChooser.setSelectedExtensionFilter(
                new FileChooser.ExtensionFilter("JPEG - File", "*.jpg")
        );
        setNewArtwork(fileChooser.showOpenDialog(((Node) (event.getSource())).getScene().getWindow()));

    }

    private void setNewArtwork(File artwork) {
        boolean accept = true;

        if (artwork != null) {
            //BMP, GIF JPEG and PNG
            List<String> extensions = Arrays.asList("bmp", "gif", "jpeg", "jpg", "png");

            String[] split = artwork.getPath().split("\\.");
            var extension = split[split.length - 1];

            if (!extensions.contains(extension.toLowerCase()))
                accept = false;
        }

        if (accept) {
            this.artwork = artwork;
            if (artwork != null) {
                this.imgArtwork.setImage(new Image(artwork.toURI().toString()));
                this.artworkChanged = true;
            }
            manageImageButtonVisibility(anime);
        }
    }

    public void initialize() {
        UnaryOperator<TextFormatter.Change> filter = change -> {
            if (change.getText().matches("[0-9]*"))
                return change;
            return null;
        };

        tf_epCount.setTextFormatter(new TextFormatter<String>(filter));
        tf_continueAt.setTextFormatter(new TextFormatter<String>(filter));

        this.tf_continueAt.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (!tf_epCount.getText().isEmpty() && !newValue.isEmpty() &&
                    Integer.valueOf(newValue) > Integer.valueOf(tf_epCount.getText())) {
                this.tf_continueAt.setText(oldValue);
                GlobalUiCollection.showNotification(this.cb_audio.getScene().getWindow(), CONTINUE_AT_MSG);
            }
        }));

        this.imgArtwork.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                event.acceptTransferModes(TransferMode.COPY);
                event.consume();
            }
        });

        this.imgArtwork.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean sucess = false;

                if (db.hasFiles()) {
                    var files = db.getFiles();

                    if (files.size() == 1) {
                        File file = files.get(0);
                        sucess = true;
                        setNewArtwork(file);
                    }
                }

                if (db.hasUrl()) {
                    System.out.println(db.getUrl());
                }

                event.setDropCompleted(sucess);
                event.consume();
            }
        });
    }

    public AnimeAddOrEditController() {

    }

    public void setup(Anime anime) {
        this.anime = anime;

        if (anime != null) {
            this.tf_name.setText(anime.getName());
            this.tf_secondName.setText(anime.getSecondName());
            this.ta_notes.setText(anime.getNotes());
            this.ta_description.setText(anime.getDescription());
            this.tbSeenYes.setSelected(anime.getSeen());
            this.tbSeenNo.setSelected(!anime.getSeen());
            this.rating.setRating((int) anime.getRating());
            this.cb_type.setValue(anime.getContentType().toString());
            this.imgArtwork.setImage(dataController.getArtwork(anime.getId()).getImage()); //TODO: Can´t locate image msg? Or keep silent?
            this.cb_audio.setValue(anime.getAudioType().toString());
            this.tf_epCount.setText(anime.getEpisodeCount() >= 0 ? String.valueOf(anime.getEpisodeCount()) : "");
            this.tf_continueAt.setText(anime.getContinueWatching() >= 0 ? String.valueOf(anime.getContinueWatching()) : "");
            manageImageButtonVisibility(anime);

            if (anime.getArtworkPath() != null && !anime.getArtworkPath().isEmpty()) {
                this.artwork = new File(anime.getArtworkPath());
            }

            //mangas ain´t have sound... duhhhh
            if (anime.getContentType() == ContentType.Manga) {
                cb_audio.getSelectionModel().select(2);
                cb_audio.setDisable(true);
            }
        } else {
            this.bt_deleteImage.setVisible(false);
            this.imgArtwork.setImage(new Image("/images/noimage.jpg"));
        }

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/settings/SourceSettings.fxml"));
        Parent parent = null;
        try {
            parent = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.sourceController = fxmlLoader.getController();

        sourceController.setup(anime);

        tabSources.setContent(parent);
    }

    public void manageImageButtonVisibility(Anime anime) {
        this.bt_deleteImage.setVisible(this.imgArtwork != null);
    }

    @FXML
    public void comboBoxTypeAction(ActionEvent actionEvent) {
        //mangas ain´t have sound... duhhhh
        if (cb_type.getValue().toString().equals(ContentType.Manga.toString())) {
            cb_audio.getSelectionModel().select(2);
            cb_audio.setDisable(true);
        } else {
            cb_audio.setDisable(false);
        }
    }

    @FXML
    public void onKeyPressedGlobal(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ESCAPE) {
            buttonCancelClicked(keyEvent);
        }
    }
}
