package fxui.controllers;

import animelist.GlobalStore;
import animelist.utility.FlacPlayer.FlacPlayer;
import fxui.GlobalUiCollection;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.stage.FileChooser;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class AboutController {

    private FlacPlayer audioWorker = new FlacPlayer();

    @FXML
    private Button btPlayToggle;

    @FXML
    private Slider playerSlider;

    private volatile boolean listenerToggle = true;

    @FXML
    public void initialize() {
        btPlayToggle.setGraphic(GlobalStore.getImageViewPause());

        if (Files.exists(Paths.get("about.flac")))
            FlacPlayer.openRequest(new File("about.flac"));

        FlacPlayer.getCurrentTime().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                listenerToggle = false;
                playerSlider.setValue(newValue.doubleValue());
                listenerToggle = true;
            }
        });

        playerSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (oldValue != newValue && listenerToggle) {
                    FlacPlayer.seekRequest(playerSlider.getValue());
                    btPlayToggle.setGraphic(GlobalStore.getImageViewPause());
                }
            }
        });
    }

    public void killAudioWorker() {
        FlacPlayer.kill();
    }

    private void openHyperlink(String urlString) {
        final String browseUrl = urlString;
        if (Desktop.isDesktopSupported()) {
            new Thread(() -> {
                try {
                    Desktop.getDesktop().browse(new URI(browseUrl));
                } catch (IOException | URISyntaxException e) {
                    Platform.runLater(() -> {
                        GlobalUiCollection.showError("IOException|URISyntaxexception", "Your URL is not formatted correctly.");
                    });
                }
            }).start();
        }
    }

    public void hyperlinkBandcamp(ActionEvent actionEvent) {
        openHyperlink("http://envmusic1.bandcamp.com");
    }

    public void hyperlinkNayuki(ActionEvent actionEvent) {
        openHyperlink("https://www.nayuki.io/page/simple-gui-flac-player-java");
    }

    public void hyperlinkMAL(ActionEvent actionEvent) {
        openHyperlink("https://myanimelist.net/modules.php?go=pluginapi");
    }

    public void buttonChooseTrack(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Track");
        fileChooser.setSelectedExtensionFilter(
                new FileChooser.ExtensionFilter("FLAC - File", "*.flac")
        );
        File file = fileChooser.showOpenDialog(((Node) (actionEvent.getSource())).getScene().getWindow());
        if (file != null) {
            FlacPlayer.openRequest(file);
            btPlayToggle.setGraphic(GlobalStore.getImageViewPause());
        }
    }

    public void btPlayPausePress(ActionEvent actionEvent) {
        btPlayToggle.setGraphic(
                FlacPlayer.togglePause() ?
                        GlobalStore.getImageViewPlay() :
                        GlobalStore.getImageViewPause());
    }
}
