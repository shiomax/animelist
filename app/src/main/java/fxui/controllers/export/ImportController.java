package fxui.controllers.export;

import animelist.io.DataConverter;
import fxui.AppStart;
import fxui.GlobalUiCollection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ImportController {

    @FXML
    private CheckBox checkIncludeImages, checkIncludeGlobalSources, checkIncludeSources;

    @FXML
    private TextField tf_path;

    @FXML
    private ComboBox cbImportFormat;

    @FXML
    private HBox hboxGlobalSources;

    @FXML
    public void btImportAction(ActionEvent actionEvent) {
        String paths = tf_path.getText();
        if(Files.exists(Paths.get(tf_path.getText()))) {
            boolean legacy = !cbImportFormat.getSelectionModel().getSelectedItem().equals("CSV-File");
            ((Node)actionEvent.getSource()).getScene().getWindow().hide();
            DataConverter.importCsvFile(new File(this.tf_path.getText()), legacy, checkIncludeImages.isSelected(),
                    checkIncludeSources.isSelected(), !legacy && checkIncludeGlobalSources.isSelected());
        } else {
            GlobalUiCollection.showNotification(((Node)actionEvent.getSource()).getScene().getWindow(),
                    "The file does not exist.");
        }
    }

    @FXML
    public void btCancelAction(ActionEvent actionEvent) {
        ((Node)actionEvent.getSource()).getScene().getWindow().hide();
    }

    @FXML
    public void btBrowseAction(ActionEvent actionEvent) {
        boolean legacy = !cbImportFormat.getSelectionModel().getSelectedItem().equals("CSV-File");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(legacy ? "Select Legacy CSV-File" : "Select CSV-File");
        fileChooser.setSelectedExtensionFilter(
                new FileChooser.ExtensionFilter("CSV", "*.csv")
        );
        var file = fileChooser.showOpenDialog(AppStart.getPrimaryStage());
        if (file != null) {
            this.tf_path.setText(file.getPath());
        } else {
            GlobalUiCollection.showNotification(((Node)actionEvent.getSource()).getScene().getWindow(),
                    "You didn't select a file.");
        }
    }

    public void initialize() {
        this.checkIncludeImages.setSelected(true);
        this.checkIncludeGlobalSources.setSelected(true);
        this.checkIncludeSources.setSelected(true);
        this.cbImportFormat.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(oldValue != newValue) {
                hboxGlobalSources.setVisible(newValue.equals("CSV-File"));
            }
        });
    }
}
