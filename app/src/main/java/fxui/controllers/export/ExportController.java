package fxui.controllers.export;

import animelist.io.ControllerFactory;
import animelist.io.DataConverter;
import fxui.GlobalUiCollection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ExportController {

    @FXML
    private CheckBox checkExportImages, checkGlobalSources, checkExportSources;

    @FXML
    private TextField tf_path;

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
    private String defaultPath;
    private LocalDateTime localDateTime;

    public void initialize() {
        localDateTime = LocalDateTime.now();
        defaultPath = String.format("exports/%s", dateTimeFormatter.format(localDateTime));
        this.tf_path.setText(defaultPath);
        this.checkExportImages.setSelected(true);
        this.checkExportSources.setSelected(true);
        this.checkGlobalSources.setSelected(true);
        this.tf_path.setEditable(false);
    }

    @FXML
    public void btCancelAction(ActionEvent actionEvent) {
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }

    @FXML
    public void btExportAction(ActionEvent actionEvent) {
        if (this.tf_path.getText().isEmpty() == false) {
            ((Node) actionEvent.getSource()).getScene().getWindow().hide();
            DataConverter.exportToCsv(ControllerFactory.getDefaultController().getAll(), localDateTime, checkExportImages.isSelected(),
                    checkExportSources.isSelected(), checkGlobalSources.isSelected());
        } else {
            GlobalUiCollection.showNotification(((Node) actionEvent.getSource()).getScene().getWindow(),
                    "Your didn´t enter a path to export to.");
        }
    }
}
