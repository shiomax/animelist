package fxui;

import animelist.GlobalStore;
import javafx.application.Preloader;
import javafx.stage.Stage;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.writers.FileWriter;
import pluginapi.PluginLoader;

import java.io.File;
import java.net.URISyntaxException;
import java.security.CodeSource;

import static com.sun.glass.ui.Application.*;

public class AppPreloader extends Preloader {
    @Override
    public void start(Stage stage) throws Exception {
        GetApplication().setName("AnimeList");

        //Set Jar path as it's needed for plugin loading when changing working directory of the app
        try {
            CodeSource codeSource = AppPreloader.class.getProtectionDomain().getCodeSource();
            File jarFile = new File(codeSource.getLocation().toURI().getPath());
            GlobalStore.setJarPath(jarFile.getParentFile().getPath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        PluginLoader pluginLoader = PluginLoader.instanceOf();

        final String LOG_FILE = "animelist.log";

        //Configuring Logger
        Configurator.defaultConfig()
                .writer(new FileWriter(LOG_FILE,false, true)) //buffered = false, append = true
                .level(Level.ERROR)
                .activate();
    }
}
