package fxui.custom;

import javafx.application.Platform;
import javafx.scene.control.Label;

import java.util.Arrays;
import java.util.List;

public class SearchingLabel extends Label {

    private static final List<String> ITERATION_LABELS = Arrays.asList(
            "searching ..",
            "searching ...",
            "searching .....",
            "searching ."
    );

    private int i;
    private labelThread labelThread = null;

    public SearchingLabel() {
        this.i = 0;
        this.setText(ITERATION_LABELS.get(i));
    }

    public void nextLabel() {
        this.setText(ITERATION_LABELS.get(
                i < 3 ? ++i : (i=0)
        ));
    }

    public void stopAnnimation() {
        if(this.labelThread != null)
            this.labelThread.setRunning(false);
    }

    public void startAnnimation() {
        this.labelThread = new labelThread();
        this.labelThread.setDaemon(true);
        this.labelThread.setRunning(true);
        this.labelThread.start();
    }

    private class labelThread extends Thread {

        private volatile boolean isRunning;

        @Override
        public void run() {
            this.isRunning = true;

            while (isRunning) {
                try {
                    Thread.sleep(250);
                    Platform.runLater(() -> {
                        nextLabel();
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void setRunning(boolean running) {
            this.isRunning = running;
        }

        public boolean isRunning() {
            return isRunning;
        }
    }
}
