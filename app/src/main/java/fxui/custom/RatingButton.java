package fxui.custom;

import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class RatingButton extends ToggleButton {

    private static final Image IMG_SELECTED = new Image("/images/hearts/red.png");
    private static final Image IMG_NOT_SELECTED = new Image("/images/hearts/blank.png");

    private ImageView selected, notSelected;

    public RatingButton() {
        super();

        this.selected = new ImageView(IMG_SELECTED);
        this.notSelected = new ImageView(IMG_NOT_SELECTED);

        this.setGraphic(this.notSelected);
        this.getStylesheets().removeAll();
        this.getStylesheets().add("/css/RatingToggle.css");

        this.selectedProperty().addListener((observable, oldValue, newValue) -> {
            setGraphic(newValue ? this.selected : this.notSelected);
        });
    }

    @Override
    public void fire() {
        super.fire();
    }
}
