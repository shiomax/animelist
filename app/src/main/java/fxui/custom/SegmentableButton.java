package fxui.custom;

import javafx.scene.Node;
import javafx.scene.control.ToggleButton;

public class SegmentableButton extends ToggleButton {
    @Override
    public void fire() {
        // don't toggle from selected to not selected if part of a group
        if (getToggleGroup() == null || !isSelected()) {
            super.fire();
        }
    }

    public SegmentableButton() {
        super();
    }

    public SegmentableButton(String text, Node graphic) {
        super(text, graphic);
    }

    public SegmentableButton(String text) {
        super(text);
    }
}
