package fxui.custom;

import javafx.scene.layout.HBox;

public class RatingControl extends HBox {

    private RatingButton[] toggleButtons = new RatingButton[5];
    private int ratingValue = 0;

    public void setRating(int rating) {
        this.ratingValue = rating;
        for (int i = 0; i < toggleButtons.length; i++)
            toggleButtons[i].setSelected(i < ratingValue);
    }

    public int getRating() {
        return ratingValue;
    }

    public RatingControl() {
        super();
        this.setSpacing(0);
        for (int i = 0; i < toggleButtons.length; i++) {
            toggleButtons[i] = new RatingButton();
            toggleButtons[i].setSelected(false);
        }
        toggleButtons[0].setOnMouseClicked(event -> {
            setRating(toggleButtons[0].isSelected() || this.getRating() > 1 ? 1 : 0);
        });
        toggleButtons[1].setOnMouseClicked(event -> {
            setRating(toggleButtons[1].isSelected() || this.getRating() > 2 ? 2 : 0);
        });
        toggleButtons[2].setOnMouseClicked(event -> {
            setRating(toggleButtons[2].isSelected() || this.getRating() > 3 ? 3 : 0);
        });
        toggleButtons[3].setOnMouseClicked(event -> {
            setRating(toggleButtons[3].isSelected() || this.getRating() > 4 ? 4 : 0);
        });
        toggleButtons[4].setOnMouseClicked(event -> {
            setRating(toggleButtons[4].isSelected() ? 5 : 0);
        });
        this.getChildren().addAll(toggleButtons);
        setRating(0);
    }
}
