#!/bin/sh

if [ -z "$1" ]; then
    echo "No Dropbox API token was supplied."
    exit 1
fi

if [ ! -z "$2" ]; then
    UPLOAD_FOLDER="$2"
else
    UPLOAD_FOLDER="AnimeList"
fi

for cur in ./package/*; do

    filename=$(echo $cur | cut -c11-)
    echo "$filename"

    if [ ${filename: -4} == ".zip" ]; then
        folder="$UPLOAD_FOLDER/zip"
    elif [ ${filename: -3} == ".gz" ]; then
        folder="$UPLOAD_FOLDER/tar"
    elif [ ${filename: -4} == ".dmg" ]; then
        folder="$UPLOAD_FOLDER/dmg"
    elif [ ${filename: -4} == ".rpm" ]; then
        folder="$UPLOAD_FOLDER/rpm"
    else
        folder="$UPLOAD_FOLDER"
    fi

    curl -X POST https://content.dropboxapi.com/2/files/upload \
        --header "$1"\
        --header "Dropbox-API-Arg: {\"path\": \"/$folder/$filename\",\"mode\": \"overwrite\",\"autorename\": false,\"mute\": true,\"strict_conflict\": false}" \
        --header "Content-Type: application/octet-stream" \
        --data-binary @"$cur"
done