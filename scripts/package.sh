#!/bin/sh

TMP_DIR="$(pwd)/package/tmp"

init() {
    if [ -d "package" ]; then
        rm -rf package/*
    else
        mkdir package
    fi

    if [ ! -d "build" ]; then
        echo "build directory does not exist, cannot package"
        exit 1
    fi

    if [ ! -f $TMP_DIR ]; then
        mkdir $TMP_DIR
    fi
}

cleanup() {
    rm -rf $TMP_DIR
}

get_resources() {
    cp icon.png build/icon.png
}

package_zip() {
    dirSave=$(pwd)
    cd build
    target="../package/animelist_$1.zip"
    latest="../package/animelist_latest.zip"
    zip -rv $target *
    if [ ! -f $target ]; then
        echo "Failed to package zip."
        exit 1
    fi
    cp -rf $target $latest
    cd "$dirSave"
}

package_tarball() {
    target="package/animelist_$1.tar.gz"
    latest="package/animelist_latest.tar.gz"
    tar -zcvf $target -C build .
    if [ ! -f $target ]; then
        echo "Failed to package tar.gz"
        exit 1
    fi
    cp -rf $target $latest
}

package_macos() {
    target="package/animelist_$1.dmg"
    latest="package/animelist_latest.dmg"

    cp -r scripts/DMG $TMP_DIR
    cp -r build/* "$TMP_DIR/DMG/AnimeList.app/Contents/MacOS"

    # Generate .icns icon
    dirSave=$(pwd)
    cd scripts
    npm install
    dest="$TMP_DIR/application"
    echo dest
    node node_modules/png2icons/png2icons-cli.js ../icon.png application -icns -bc -i
    mkdir $TMP_DIR/DMG/AnimeList.app/Contents/Resources
    mv application.icns $TMP_DIR/DMG/AnimeList.app/Contents/Resources
    cd "$dirSave"

    genisoimage -D -V "AnimeList 0.1" -no-pad -r -apple -o $target "$TMP_DIR/DMG"

    cp $target $latest
}

package_rpm() {
    target="package/animelist_$1.rpm"
    latest="package/animelist_latest.rpm"

    
}

init
get_resources
timestr=$(date +%d-%m-%Y_%H-%M-%S)
package_zip $timestr
package_tarball $timestr
package_macos $timestr
package_rpm $timestr
cleanup