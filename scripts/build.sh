#!/bin/sh

# Variables
APP_BUILD=app/target/app-1.0-SNAPSHOT-jar-with-dependencies.jar
APP_TARGET=build/AnimeList.jar
ANILIST_BUILD=app/target/plugins/anilist-1.0-SNAPSHOT-jar-with-dependencies.jar
ANILIST_TARGET=build/plugins/AniListPlugin.jar
OMDB_BUILD=app/target/plugins/omdb-1.0-SNAPSHOT-jar-with-dependencies.jar
OMDB_TARGET=build/plugins/OmdbPlugin.jar

init_cleanup() {
    # Clean build directory
    if [ -d "build" ]; then
        rm -rf build/*
    else
        mkdir build
    fi

    # Just print versions to see in debug log
    mvn -version
    java -version
}

build_jars() {
    # Clean package
    mvn clean package -B \
        -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn

    # Copy to output directory
    cp $APP_BUILD $APP_TARGET
    mkdir build/plugins
    cp $ANILIST_BUILD $ANILIST_TARGET
    cp $OMDB_BUILD $OMDB_TARGET
}

init_cleanup
build_jars