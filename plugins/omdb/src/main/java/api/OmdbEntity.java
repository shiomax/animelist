package api;

import apicontracts.IApiEntity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class OmdbEntity implements IApiEntity {

    private List<String> nameOptions = new LinkedList<>();

    private String description = "";

    private String poster = null;

    @Override
    public List<String> getNameOptions() {
        return nameOptions;
    }

    @Override
    public List<String> getImageOptions() {
        return poster == null ? null : Arrays.asList(poster);
    }

    @Override
    public List<String> getDescriptionOptions() {
        return Arrays.asList(description);
    }

    public void addNameOption(String name) {
        this.nameOptions.add(name);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setArtwork(String artwork) {
        this.poster = artwork;
    }
}
