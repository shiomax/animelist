package api;

import apicontracts.*;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class OmdbClient extends ApiClient {

    private static final String OMDB_API_SETTINGS_KEY = "omdbapikey";

    /*
     * Search for movies and series
     * */
    private static final String URL_SEARCH = "http://www.omdbapi.com/?s=%s&apikey=%s";

    /*
     * Details to one entry selected with imdbID
     * */
    private static final String URL_DETAILS = "http://www.omdbapi.com/?i=%s&apikey=%s";

    public OmdbClient() {
        super(
                Arrays.asList(
                        new ApiSetting("Api-Key", OMDB_API_SETTINGS_KEY, SettingDisplayType.text)
                )
        );
    }

    @Override
    public SearchResult search(String s) {

        String key = KeyValueStore.instanceOf().get(OMDB_API_SETTINGS_KEY);

        if (key == null || key.isEmpty())
            return new SearchResult(new LinkedList<>(), false,
                    "API key not configured. Goto settings and enter a valid ApiKey for OmDb!");

        List<IApiEntity> results = new LinkedList<>();

        var client = ClientBuilder.newClient()
                .target(String.format(URL_SEARCH, URLEncoder.encode(s, StandardCharsets.UTF_8), key));

        Response response = null;

        try {
            response = client.request(MediaType.APPLICATION_JSON).get();
        } catch (javax.ws.rs.ProcessingException ex) {
            return new SearchResult(results, false,
                    "Network related error!");
        }

        if(response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode())
            return new SearchResult(results, false,
                    "Authorization failed. Goto settings and enter a valid ApiKey for OmDb!");

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            var str = response.readEntity(String.class);

            JsonParser parser = new JsonParser();
            JsonObject json = parser.parse(str).getAsJsonObject();

            //Found entries
            if (json.has("Search")) {

                var searchObj = json.getAsJsonArray("Search");

                for (var cur : searchObj) {

                    var curJson = cur.getAsJsonObject();
                    var entity = new OmdbEntity();

                    if (curJson.has("Title")) {
                        entity.addNameOption(curJson.get("Title").getAsString());
                    }
                    if (curJson.has("Poster")) {
                        var poster = curJson.get("Poster");
                        entity.setArtwork(poster.isJsonNull() ? "" : poster.getAsString());
                    }

                    if (curJson.has("imdbID")) {
                        client = ClientBuilder.newClient()
                                .target(String.format(URL_DETAILS,
                                        curJson.get("imdbID").getAsString(),
                                        key));

                        response = client.request(MediaType.APPLICATION_JSON).get();

                        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
                            str = response.readEntity(String.class);

                            var innerJson = parser.parse(str).getAsJsonObject();

                            if (innerJson.has("Plot")) {
                                var plot = innerJson.get("Plot");
                                entity.setDescription(plot.getAsString());
                            }
                        }
                    }

                    results.add((IApiEntity) entity);
                }

            } else {
                return new SearchResult(results, true, "No results.");
            }
        } else {
            return new SearchResult(results, false,
                    "Search failed with status code "
                            + response.getStatus() + ".");
        }

        return new SearchResult(results, true, null);
    }

    @Override
    public String getIdentifier() {
        return "OMDbApi";
    }

    @Override
    public String getDisplayName() {
        return "OMDbApi";
    }
}
