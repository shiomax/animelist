package api;

import apicontracts.IApiEntity;

import java.util.List;

public class AniListEntity implements IApiEntity {

    private List<String> nameOptions;

    private List<String> imageOptions;

    private List<String> descriptionOptions;

    @Override
    public List<String> getNameOptions() {
        return nameOptions;
    }

    @Override
    public List<String> getImageOptions() {
        return imageOptions;
    }

    @Override
    public List<String> getDescriptionOptions() {
        return descriptionOptions;
    }

    public AniListEntity(List<String> nameOptions, List<String> imageOptions, List<String> descriptionOptions) {
        this.nameOptions = nameOptions;
        this.imageOptions = imageOptions;
        this.descriptionOptions = descriptionOptions;
    }
}
