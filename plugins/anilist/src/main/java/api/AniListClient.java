package api;

import apicontracts.*;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AniListClient extends ApiClient {

    private static final String API_QUERY =
            "query ($page: Int, $perPage: Int, $search: String) {\n" +
            "  Page (page: $page, perPage: $perPage) {\n" +
            "    pageInfo {\n" +
            "      total\n" +
            "      currentPage\n" +
            "      lastPage\n" +
            "      hasNextPage\n" +
            "      perPage\n" +
            "    }\n" +
            "    media (search: $search) {\n" +
            "      title {\n" +
            "        romaji,\n" +
            "        english,\n" +
            "        native\n" +
            "      }\n" +
            "      description\n" +
            "      coverImage {\n" +
            "        large\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    private static final String API_VARIBLAES =
            "{\n" +
            "\"search\" : \"%s\",\n" +
            "\"page\" : 1,\n" +
            "\"perPage\" : 100\n" +
            "}";


    private static final String API_URL = "https://graphql.anilist.co";

    @Override
    public SearchResult search(String s) {

        List<IApiEntity> responseList = new LinkedList<>();

        var client = ClientBuilder.newClient()
                .target(API_URL);

        var form = new Form()
                .param("query", API_QUERY)
                .param("variables", String.format(API_VARIBLAES, s));

        Response response = null;

        try {
            response = client.request(MediaType.APPLICATION_JSON)
                    .post(Entity.form(form));
        }catch (javax.ws.rs.ProcessingException ex) {
            return new SearchResult(responseList, false,
                    "Network related error!");
        }

        if(response.getStatus() == Response.Status.OK.getStatusCode()) {

            var str = response.readEntity(String.class);

            JsonParser parser = new JsonParser();
            JsonObject json = parser.parse(str).getAsJsonObject();

            if(json.has("data")) {
                var pageObj = json.getAsJsonObject("data").getAsJsonObject("Page");

                var mediaArray = pageObj.getAsJsonArray("media");

                for(var cur : mediaArray) {
                    var curObj = cur.getAsJsonObject();

                    var desJson = curObj.get("description");
                    String description = desJson.isJsonNull() ? "" : desJson.getAsString();

                    List<String> titles = new LinkedList<>();

                    curObj.get("title").getAsJsonObject().entrySet().forEach(entry -> {
                        if(!entry.getValue().isJsonNull())
                            titles.add(entry.getValue().getAsString());
                    });

                    List<String> images = new LinkedList<>();

                    curObj.get("coverImage").getAsJsonObject().entrySet().forEach( entry -> {
                        if(!entry.getValue().isJsonNull())
                            images.add(entry.getValue().getAsString());
                    });

                    responseList.add(new AniListEntity(titles, images, Arrays.asList(description)));
                }
            }
        } else {
            return new SearchResult(responseList, false,
                    "Search failed with status code "
                            + response.getStatus() + ". ");
        }

        return new SearchResult(responseList, true, null);
    }

    @Override
    public String getIdentifier() {
        return "anilistapi";
    }

    @Override
    public String getDisplayName() {
        return "AniList";
    }
}
