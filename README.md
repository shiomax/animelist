# AnimeList

This is a Mediamanagement Software, I´ve written for a friend as a practice/pet project.

Mostly targeted towards Animes. But can also be used for Mangas, Series and Movies.
The best way to describe it would be imo it´s a 'glorified bookmarker', as that was the 'problem' to be solved. 
That friend, I was referring to earlier, had all his Animes and Mangas he didn´t yet watch as bookmarks in his browser and believe me, there where **lots** of them.

If you´re looking for a way to keep track of what you´re watching without relying on a Website then this might be sufficient. 
It is also fully portable, so you can put it into Dropbox, or similar Cloud storage and Synchronize it between your PCs that way.

As far as the 'glorified bookmarking' goes, you have two types of bookmarks. GlobalSources and SpecificSources. 
One of them you set in your Settings menu and it will be available for every Entry (as long as it´s not disabled for the Type of Entry).
The other one is specified specifically for one entry.
You can also add all the fields of the Anime class as URL parameters, like so ```https://www.netflix.com/search?q=%name%```.
While in theory this works with all fields, I believe it only really makes sence with the following
- name
- secondName
- continueWatching

The program can also update metadata for your entries, fetching Data from wherever the plugin gets the Data from. Currently, I wrote two plugins myself for Anilist and OmDb.

# Screenshots

![Screenshot 1](https://uploads.shio.at/animelist/1.png)
![Screenshot 2](https://uploads.shio.at/animelist/2.png)

# Downloads

**All the new Downloads can found [here](https://www.dropbox.com/sh/fdjr7sboudy52ot/AAC2sclkRTvUULh2ZbeQgBSZa?dl=0).**
Before I packaged the zip files manually and uploaded them. Now, they are automatically packaged by gilab-ci and uploaded to dropbox.
Plus, they also come in zip, tar.gz and dmg variety intended for Windows, Linux and Mac respectively.

Some of the old zip Downloads can still be found below for the time being, but they will go away eventually.

**Snapshot 01.06.2019:** [Download](https://uploads.shio.at/animelist/snapshot010619.zip)<br>
**Snapshot 27.05.2019:** [Download](https://uploads.shio.at/animelist/snapshot270519.zip)<br>
**Snapshot 01.03.2019:** [Download](https://uploads.shio.at/animelist/snapshot010319.zip)

NOTE: All of these are compiled with JDK11 for JDK11. Earlier versions of Java won´t work at all. But later version are expected to work (fingers crossed).

It works on Windows, Linux and MacOS. Since, JavaFX is now no longer part of OraclesJDK, the native binaries
are bundled into the jar itself. This also means you can now use OpenJDK.
I tested it on Windows, Linux and Mac.

# Installation

## Windows

Download the latest zip [here](https://www.dropbox.com/sh/fdjr7sboudy52ot/AACGBdWi68B96mIed1C8JjqZa/zip/animelist_latest.zip?dl=1).

All you need is a Version of Java11 or higher, currently I'd say install 
[OracleJDK12](https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html).
By the time you read this there maybe a newer version of Java around. Oracle seems to sort of 'archive' old versions of Java at this point,
by requiering an oracle account to download them. For the newest version at any given time you don´t. If there is a newer version than 12 around, you should find it 
[here](https://www.oracle.com/technetwork/java/javase/downloads/index.html).

You need to make sure that the Java Version is set as default. If you still need a Version earlier that Java11 as your default.
You can create a file within the folder where ```AnimeList.jar``` resides, call it ```AnimeList.bat``` and put the following inside.
```
@echo off
set path=C:\Program Files\Java\jdk-12.0.1\bin\;%path%
java -version
START javaw -jar AnimeList.jar
exit
```

Now you can launch the program with this ```.bat``` file.

NOTE: Change ```C:\Program Files\Java\jdk-12.0.1\bin\``` to where your Java install is.

## Linux (Fedora29/30)

1. Install OpenJDK 11
```
sudo dnf install java-11-openjdk
```
2. Select as default (otherwise you need to specify the full path of your java11 installtion in your .desktop file)
```
sudo alternatives --config java
```
3. Check default java version (should say 11)
```
java -version
```

4. Make directory in opt and change owner to your user
```
sudo mkdir /opt/AnimeList && sudo chown $USER:$USER /opt/AnimeList
```

5. Download and untar latest version 
```
wget -O - "https://www.dropbox.com/sh/fdjr7sboudy52ot/AADgX5jPu1rshfqt-6NZaRw_a/tar/animelist_latest.tar.gz?dl=1" | tar xvz -C /opt/AnimeList
```

NOTE: With this command you can also update the program later on, you won't need to do all the rest again.

6. Create .desktop file
```
cat <<EOF | sudo tee /usr/local/share/applications/animelist.desktop
[Desktop Entry]
Version=0.1
Type=Application
Name=AnimeList
Path=/opt/AnimeList
Icon=/opt/AnimeList/icon.png
Exec=java -jar AnimeList.jar
Terminal=false
Categories=Utility
EOF
```

## MacOS

As with the Windows install, you'll have to install a version of JDK11 or higher. OraclesJDK or OpenJDK does not matter. OraclesJDK is probably
the easier route. But I think if you're using [brew](https://brew.sh/) it probably gives you a good option to install and configure OpenJDK as well.

You can download the latest dmg [here](https://www.dropbox.com/sh/fdjr7sboudy52ot/AAAXyU44RBUlwl3ry9Wx-LZEa/dmg/animelist_latest.dmg?dl=1).

Mount the dmg. Drag the app inside into your applications.

However, there is one catch. As I do not have an apple developer account, I cannot sign the app. 
Hence it will tell you the app was downloaded from the internet from an unindentified source/developer. 
Usually, you can right click the app (right click not double click) and then click open and it will give you an
option to 'open it anyway' (you don't get that option if you double click). But you only have to do that once.

AnimeList will save all it's data into ```~/.animelist``` and not into the directory where the jar resides as it's the case with all the other installs
currently.